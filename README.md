# BezierComposer

Compose continuous voices using Bézier curves.

## Features

* Composition of continuous voices
* Full control with Bézier curves
* Realtime output using JACK (CV/Gate) or Open Sound Control (frequency & gate)
* Sync to other software using Jack Transport
* Polyphonic: create multiple tracks
* Flexible: Connect it to any synthesizer that supports CV/Gate, including most
  audio programming languages, for example SuperCollider, CSound, FAUST, ...

## Getting Started

To use this software, you need the [JACK Audio Connection Kit](https://jackaudio.org/).
To actually produce sounds, you must connect the BezierComposer to a synthesizer via JACK
or OSC, the examples section includes a setup using SuperCollider.

## Installation

### Prerequisites

The software should be portable, but is only tested on Linux and instructions can only be provided for Linux.

To build from source, start by installing the necessary dependencies.

### Dependencies

BezierComposer depends on

* [Jack](https://jackaudio.org/) - Transport and CV/Gate output
* [gtkmm](https://gtkmm.org/) - C++ interfaces for GTK+
* [pugixml](https://pugixml.org/) - XML to save/load files
* [oscpack](http://www.rossbencina.com/code/oscpack) - OSC output

On a Debian-based distribution, the following command should install the necessary dependencies:

```
$ sudo apt-get install libjack-jackd2-dev libgtkmm-3.0-dev libpugixml-dev liboscpack-dev
```

### Building from a tarball

Download the tarball you want to install, then run
```
$ tar xf beziercomposer-x.x.x.tar.xz
$ cd bedziercomposer-x.x.x
$ ./configure
$ make
$ sudo make install
```

### Building from Git

This program uses autotools, so make sure you have autotools installed if you want to build directly
from the repository and not from a tarball.
```
$ git clone https://codeberg.org/BezierComposer/BezierComposer.git
$ cd BezierComposer
$ autoreconf -i
$ ./configure
$ make
$ sudo make install
```

## Composing

### Startup

First setup and start your JACK server using qjackctl or a similar tool. Then launch BezierComposer,
it will automatically connect to the JACK server. By now, you won't be able to hear anything, see
the Getting Sound section below for instructions on how to actually produce sound.

### Keyboard shortcuts

Besides the standard shortcuts, which can be found in the menu bar, there are:

* Ctrl+T: Add track
* Ctrl+Right Click: Add curve
* Ctrl+Delete: Remove selected curves
* Delete: Remove selected points
* Right click: Add point to selected curve
* c: connect two points (endpoint and startpoint)
* s: disconnect two points
* Scrolling left of the keyboard: change the height of the corresponding track
* Scrolling right of the keyboard: move the range of the corresponding track up/down
* Ctrl+Scroll: change the horizontal zoom
* Shift+Scroll: scroll horizontally
* Ctrl+Alt+Scroll: increase/decrease the range of the corresponding track
* g: retriggering on/off for selected points
* s: snap on/off
* Ctrl+Click & Shift+Click: selected multiple points


### Basic operations

For each voice, a new track has to be created (Ctrl+T). Create a new curve by using (Ctrl+Right Click) and add
points to the currently selected curve using (Right Click).

The curve can be modified by dragging the handles of the corresponding points.

A retrigger signal (=Note On) can be send at a point by activating the gate property (g),

### Getting Sound

Each track provides two JACK output ports: "BezierComposer:Track n/freq_cv" and "BezierComposer:Track n/gate_cv".

freq_cv gives a control voltage similar to the ones used for modular synthesizers, starting from 0V for C0
and using 1V/octave (MIDI note / 12). gate_cv is 1 if a note is played and 0 if not, with a short retrigger
if gate is active for a point.

The outputs have to be connected to the corresponding inputs of the synthesis software
(for example by using qjackctl). The [examples](examples) directory contains the file BasicOscillatorCV for
[SuperCollider](examples/supercollider/BasicOscillatorCV.scd),
[FAUST](examples/FAUST/BasicOscillatorCV.dsp) and [CSound](examples/CSound/BasicOscillatorCV.csd).
It shows how to convert the control signal to a frequency and feed it into an oscillator.
For more tracks, open more inputs and create multiple oscillators to play from them.

If OSC is enabled (see preferences), for each track the messages "/channel{n}/freq" and "/channel{n}/gate"
will be sent. Gate is only sent when it changes. See the preferences for server address/port and the rate
of the messages. See the examples directory for a setup using SuperCollider.

### Saving files

THe files are stored in an XML file using the file ending `.bezier`

## Contributing

Any help is appreciated, feel free to submit bugs, pull requests, feature requests or provide a translation
(see po directory).

## Authors

* **Valentin Pratz** - *Initial work*

## License

This project is licensed under the GPLv3 License - see the [LICENSE](LICENSE) file for details
