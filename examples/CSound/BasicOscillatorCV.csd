/* 
Simple example: Convert the control voltage to a frequency
and use it with the gate to control a simple sawtooth oscillator
*/

<CsoundSynthesizer>
<CsOptions>
</CsOptions>
<CsInstruments>
ksmps=8
sr = 48000
nchnls = 2

instr 1
	aincv inch 1
	aingate inch 2
	kincv downsamp aincv / 32768.0 ; use control rate for our controls
	kingate downsamp aingate / 32768.0 ; the input is normalized to 32768.0
	kcps cpsmidinn 12 * kincv ; convert midi note (12 * kincv) to frequency
	printks "%.2f %.2f\r", 0.05, kcps, kingate ; print current values
	aOsc	 vco2 0dbfs / 4 * kingate, kcps, 0	; vco2 with sawtooth at -12dB FS
	out aOsc
endin

</CsInstruments>
<CsScore>
i 1 0 3600
e
</CsScore>
</CsoundSynthesizer>
