/*
 * Copyright (C) 2021 Valentin Pratz <git@valentinpratz.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef COMPOSER_H
#define COMPOSER_H

#include "composition.h"
#include "editor_area.h"
#include <gtkmm/button.h>
#include <gtkmm/applicationwindow.h>
#include <gtkmm/box.h>
#include <gtkmm.h>
// #include "stk/BlitSaw.h"
#include <jack/jack.h>
#include "pugixml.hpp"

class BezierComposer : public Gtk::ApplicationWindow
{

public:
    BezierComposer(const Glib::RefPtr<Gtk::Application>& app);
    virtual ~BezierComposer();

    static BezierComposer* create(const Glib::RefPtr<Gtk::Application>& app);

    void on_play_button_pressed();
    void on_pause_button_pressed();
    void on_goto_start_button_pressed();
    void on_toggle_play();
    void on_toggle_snap();
    void on_toggle_loop();
    void on_new_file();
    void on_open_file();
    void on_save_file();
    void on_save_file_as();
    double get_time(jack_nframes_t frame_time);
    jack_nframes_t get_frame(double time);

    Glib::RefPtr<Gio::SimpleAction> snap_action;
    Glib::RefPtr<Gio::SimpleAction> loop_action;
    Glib::RefPtr<Gio::Settings> settings;

protected:

    bool on_timeout();
    bool on_osc_timeout();
    void send_osc_msgs();
    void osc_stop_all_notes();
    void load_file(const Glib::RefPtr<Gio::File>& file);
    void setup_loaded_xml(const pugi::xml_document& doc);
    void save_file(const Glib::RefPtr<Gio::File>& file);
    static int jack_sync_callback(jack_transport_state_t state, jack_position_t *pos, void *arg);
    static void on_jack_shutdown(void *arg);
    static int on_jack_sample_rate(jack_nframes_t nframes, void *arg);
    static int jack_tick(jack_nframes_t nframes, void *arg);

    void on_setting_changed(Glib::ustring key);

    Composition composition;

    Glib::RefPtr<Gtk::Application> app;
    Gtk::Box vbox;
    Gtk::Toolbar* toolbar;

    double frame_rate;
    bool osc_enabled;
    const char* osc_server_address = "127.0.0.1";
    int osc_server_port;
    int osc_rate;

    sigc::slot<bool> gui_timeout_slot;
    sigc::slot<bool> osc_timeout_slot;
    sigc::connection gui_conn;
    sigc::connection osc_conn;

    EditorArea editor;
    jack_client_t *jack_client = nullptr;
    jack_port_t *output_port_0 = nullptr;
    jack_port_t *output_port_1 = nullptr;
    // std::list<stk::BlitSaw> sines;

    int jack_client_state = JackTransportStopped;
    int prev_osc_transport_state = JackTransportStopped;
    int osc_transport_state = JackTransportStopped;
    jack_nframes_t frame_time = 0;
    jack_nframes_t osc_frame_time = 0;
    jack_nframes_t prev_osc_frame_time = -1;

    Glib::RefPtr<Gio::File> file_ptr;
};
#endif  // COMPOSER_H
