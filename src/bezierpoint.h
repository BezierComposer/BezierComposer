/*
 * Copyright (C) 2021 Valentin Pratz <git@valentinpratz.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef BEZIERPOINT_H
#define BEZIERPOINT_H
/*
Stores information about a bezier point, the handles pre_rel_ and post_rel are relative
coordinates to x and y. If gate is true, a retrigger signal will be sent.
*/
class BezierPoint
{

public:
    BezierPoint(long id, double x=0, double y=0, double pre_rel_x=0, double pre_rel_y=0, double post_rel_x=0, double post_rel_y=0);

    // used to differentiate between the point and its handles
    enum PointType { POINT_NONE, POINT_MAIN, POINT_HANDLE_PRE, POINT_HANDLE_POST };

    long id;
    double x, y, pre_rel_x, pre_rel_y, post_rel_x, post_rel_y;
    bool gate = false;
};
#endif // BEZIERPOINT_H
