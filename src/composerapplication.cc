/*
 * Copyright (C) 2021 Valentin Pratz <git@valentinpratz.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "composerapplication.h"
#include "composer.h"
#include "composerprefs.h"
#include <iostream>
#include <exception>

ComposerApplication::ComposerApplication()
: Gtk::Application("de.wavel.beziercomposer", Gio::APPLICATION_HANDLES_OPEN)
{
}

Glib::RefPtr<ComposerApplication> ComposerApplication::create()
{
  return Glib::RefPtr<ComposerApplication>(new ComposerApplication());
}

BezierComposer* ComposerApplication::create_appwindow()
{
  auto appwindow = BezierComposer::create(Glib::RefPtr<ComposerApplication>(this));

  // Make sure that the application runs for as long this window is still open.
  add_window(*appwindow);

  // Gtk::Application::add_window() connects a signal handler to the window's
  // signal_hide(). That handler removes the window from the application.
  // If it's the last window to be removed, the application stops running.
  // Gtk::Window::set_application() does not connect a signal handler, but is
  // otherwise equivalent to Gtk::Application::add_window().

  // Delete the window when it is hidden.
  appwindow->signal_hide().connect(sigc::bind<Gtk::Window*>(sigc::mem_fun(*this,
    &ComposerApplication::on_hide_window), appwindow));

  return appwindow;
}

void ComposerApplication::on_startup()
{
    // Call the base class's implementation.
    Gtk::Application::on_startup();

    // Add actions and keyboard accelerators for the application menu.
    add_action("preferences", sigc::mem_fun(*this, &ComposerApplication::on_action_preferences));
    add_action("quit", sigc::mem_fun(*this, &ComposerApplication::on_action_quit));
    set_accel_for_action("app.quit", "<Ctrl>Q");
}

void ComposerApplication::on_activate()
{
  try
  {
    // The application has been started, so let's show a window.
    auto appwindow = create_appwindow();
    appwindow->present();
  }
  // If create_appwindow() throws an exception (perhaps from Gtk::Builder),
  // no window has been created, no window has been added to the application,
  // and therefore the application will stop running.
  catch (const Glib::Error& ex)
  {
    std::cerr << "ComposerApplication::on_activate(): " << ex.what() << std::endl;
  }
  catch (const std::exception& ex)
  {
    std::cerr << "ComposerApplication::on_activate(): " << ex.what() << std::endl;
  }
}

void ComposerApplication::on_hide_window(Gtk::Window* window)
{
  delete window;
}

void ComposerApplication::on_action_preferences()
{
  try
  {
    auto prefs_dialog = ComposerPrefs::create(*get_active_window());
    prefs_dialog->present();

    // Delete the dialog when it is hidden.
    prefs_dialog->signal_hide().connect(sigc::bind<Gtk::Window*>(sigc::mem_fun(*this,
      &ComposerApplication::on_hide_window), prefs_dialog));
  }
  catch (const Glib::Error& ex)
  {
    std::cerr << "ComposerApplication::on_action_preferences(): " << ex.what() << std::endl;
  }
  catch (const std::exception& ex)
  {
    std::cerr << "ComposerApplication::on_action_preferences(): " << ex.what() << std::endl;
  }
}

void ComposerApplication::on_action_quit()
{
  // Gio::Application::quit() will make Gio::Application::run() return,
  // but it's a crude way of ending the program. The window is not removed
  // from the application. Neither the window's nor the application's
  // destructors will be called, because there will be remaining reference
  // counts in both of them. If we want the destructors to be called, we
  // must remove the window from the application. One way of doing this
  // is to hide the window. See comment in create_appwindow().
  auto windows = get_windows();
  for (auto window : windows)
    window->hide();

  // Not really necessary, when Gtk::Widget::hide() is called, unless
  // Gio::Application::hold() has been called without a corresponding call
  // to Gio::Application::release().
  quit();
}
