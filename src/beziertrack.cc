/*
 * Copyright (C) 2021 Valentin Pratz <git@valentinpratz.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "beziertrack.h"

#include <cmath>
#include <iostream>

BezierTrack::BezierTrack(long id, jack_client_t* jack_client, bool register_ports) : id(id), jack_client(jack_client), curves(), channel(0), volume(1.0), type(NOTES), height(600), y_min(50), y_max(70)
{
    if (register_ports) {
        register_jack_ports();
    }
};

std::tuple<bool, double> BezierTrack::get_value(double time)
{
    bool value_available;
    double value;

    std::list<BezierCurve*>::iterator c_it = curves.begin();
    while (c_it != curves.end()) {
        std::tie(value_available, value) = (*c_it++)->get_value(time);
        if (value_available)
            return std::make_tuple(value_available, value);
    }
    return std::make_tuple(false, 0.0);
}

bool BezierTrack::contains_gate(double start_time, double end_time)
{
    // TODO: inefficient
    BezierCurve *start_curve = nullptr;
    BezierCurve *end_curve = nullptr;
    bool value_available;
    double value;
    std::list<BezierCurve*>::iterator c_it = curves.begin();
    while (c_it != curves.end()) {
        std::tie(value_available, value) = (*c_it)->get_value(start_time);
        if (value_available)
            start_curve = *c_it;
        std::tie(value_available, value) = (*c_it)->get_value(end_time);
        if (value_available)
            end_curve = *c_it;
        if (start_curve && end_curve) {
            if (start_curve == end_curve)
                return end_curve->contains_gate(start_time, end_time);
            else
                return true;
        }
        c_it++;
    }
    if (end_curve && !start_curve)
        return true;
    return false;
}

void BezierTrack::register_jack_ports(bool use_sound_output, bool use_trig_output)
{
    if (!jack_client) {
        std::cout << "BezierTrack: Jack Client not started(jack_client==nullptr), can't register ports" << std::endl;
        return;
    }
    if (use_sound_output)
        freq_snd_port = jack_port_register(jack_client, ("Track " + std::to_string(id) + "/freq").c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    freq_cv_port = jack_port_register(jack_client, ("Track " + std::to_string(id) + "/freq_cv").c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    gate_cv_port = jack_port_register(jack_client, ("Track " + std::to_string(id) + "/gate_cv").c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    if (use_trig_output)
        trig_cv_port = jack_port_register(jack_client, ("Track " + std::to_string(id) + "/trig_cv").c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
}

void BezierTrack::unregister_jack_ports() {
    if (freq_snd_port) jack_port_unregister(jack_client, freq_snd_port);
    freq_snd_port = nullptr;
    if (freq_cv_port) jack_port_unregister(jack_client, freq_cv_port);
    freq_cv_port = nullptr;
    if (gate_cv_port) jack_port_unregister(jack_client, gate_cv_port);
    gate_cv_port = nullptr;
    if (trig_cv_port) jack_port_unregister(jack_client, trig_cv_port);
    trig_cv_port = nullptr;
}

double BezierTrack::to_track_x(double time, double units_per_beat, double time_offset)
{
   return (time - time_offset) * units_per_beat;
}

double BezierTrack::to_track_y(double value)
{
    // for using frequencies: return (freq_to_midi(value) - freq_to_midi(y_min)) / ((freq_to_midi(y_max) - freq_to_midi(y_min)) / height);
    return (value - y_min) / ((y_max - y_min) / height);
}

std::tuple<double, double> BezierTrack::to_track_coords(double time, double value, double units_per_beat, double time_offset)
{
    double x_track = to_track_x(time, units_per_beat, time_offset);
    double y_track = to_track_y(value);
    return std::make_tuple(x_track, y_track);
}

double BezierTrack::x_track_to_time(double x_track, double units_per_beat, double time_offset)
{
    return x_track / units_per_beat + time_offset;
}

double BezierTrack::y_track_to_value(double y_track)
{
    // for using frequencies: return midi_to_freq(y_track * ((freq_to_midi(y_max) - freq_to_midi(y_min)) / height) + freq_to_midi(y_min));
    return y_track * ((y_max - y_min) / height) + y_min;
}

std::tuple<double, double> BezierTrack::to_time_value(double x_track, double y_track, double units_per_beat, double time_offset)
{
    double time = x_track_to_time(x_track, units_per_beat, time_offset);
    double value = y_track_to_value(y_track);

    return std::make_tuple(time, value);
}

std::tuple<BezierPoint::PointType, BezierPoint*, BezierCurve*, BezierTrack*> BezierTrack::in_point(double x_track, double y_track, double units_per_beat, double time_offset, double handle_rad) {
    std::list<BezierCurve*>::iterator c_it = curves.begin();
    while (c_it != curves.end()) {
        BezierCurve *curve = *c_it++;
        std::list<BezierPoint*> &points = curve->points;
        std::list<BezierPoint*>::iterator it = points.begin();

        double x_min, x_max, pre_x_min, pre_x_max, post_x_min, post_x_max;
        double y_min, y_max, pre_y_min, pre_y_max, post_y_min, post_y_max;

        while(it != points.end())
        {
            BezierPoint &p = **it++;
            x_min = to_track_x(p.x, units_per_beat, time_offset) - handle_rad;
            x_max = x_min + 2 * handle_rad;
            y_min = to_track_y(p.y) - handle_rad;
            y_max = y_min + 2 * handle_rad;
            if ( x_min < x_track && x_track < x_max && y_min < y_track && y_track < y_max )
                return std::make_tuple(BezierPoint::POINT_MAIN, &p, curve, this);
            pre_x_min = to_track_x(p.x + p.pre_rel_x, units_per_beat, time_offset)- handle_rad;
            pre_x_max = pre_x_min + 2 * handle_rad;
            pre_y_min = to_track_y(p.y + p.pre_rel_y) - handle_rad;
            pre_y_max = pre_y_min + 2 * handle_rad;
            if ( pre_x_min < x_track && x_track < pre_x_max && pre_y_min < y_track && y_track < pre_y_max )
                return std::make_tuple(BezierPoint::POINT_HANDLE_PRE, &p, curve, this);
            post_x_min = to_track_x(p.x + p.post_rel_x, units_per_beat, time_offset) - handle_rad;
            post_x_max = post_x_min + 2 * handle_rad;
            post_y_min = to_track_y(p.y + p.post_rel_y) - handle_rad;
            post_y_max = post_y_min + 2 * handle_rad;
            if ( post_x_min < x_track && x_track < post_x_max && post_y_min < y_track && y_track < post_y_max )
                return std::make_tuple(BezierPoint::POINT_HANDLE_POST, &p, curve, this);
        }
    }
    return std::make_tuple(BezierPoint::POINT_NONE, nullptr, nullptr, nullptr);

}

double BezierTrack::midi_to_freq ( double d )
{
    return std::pow(2, (d-69) / 12) * 440;
}

double BezierTrack::freq_to_midi ( double freq )
{
    return 69 + 12 * std::log2(freq / 440);
}

std::tuple<int, int> BezierTrack::freq_to_midi_pitch_bend ( double freq )
{
    double frac_midi_note = freq_to_midi( freq );
    int midi_note = std::round(frac_midi_note);
    double fract = frac_midi_note - midi_note;

    int pitch_bend = (int) (fract*8192 / 2);
    return std::make_tuple(midi_note, pitch_bend);
}
