/*
 * Copyright (C) 2021 Valentin Pratz <git@valentinpratz.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef COMPOSERPREFS_H
#define COMPOSERPREFS_H

#include <gtkmm.h>

class ComposerPrefs : public Gtk::Dialog
{
public:
  ComposerPrefs(BaseObjectType* cobject,
    const Glib::RefPtr<Gtk::Builder>& ref_builder);

  static ComposerPrefs* create(Gtk::Window& parent);

protected:
  Glib::RefPtr<Gtk::Builder> ref_builder;
  Glib::RefPtr<Gio::Settings> settings;
  Gtk::ComboBoxText* guiplaybackfps;
  Gtk::CheckButton* enableosc;
  Gtk::Entry* oscserveraddress;
  Gtk::SpinButton* oscserverport;
  Gtk::ComboBoxText* oscupdaterate;
};

#endif /* COMPOSERPREFS_H */
