/*
 * Copyright (C) 2021 Valentin Pratz <git@valentinpratz.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "composerprefs.h"
#include <stdexcept>

ComposerPrefs::ComposerPrefs(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& ref_builder)
                : Gtk::Dialog(cobject), ref_builder(ref_builder), settings(),
                guiplaybackfps(nullptr), enableosc(nullptr), oscserveraddress(nullptr),
                oscserverport(nullptr), oscupdaterate(nullptr)
{

    ref_builder->get_widget("guiplaybackfps", guiplaybackfps);
    if (!guiplaybackfps)
        throw std::runtime_error("No \"guiplaybackfps\" object in prefs.ui");

    ref_builder->get_widget("enableosc", enableosc);
    if (!enableosc)
        throw std::runtime_error("No \"enableosc\" object in prefs.ui");

    ref_builder->get_widget("oscserveraddress", oscserveraddress);
    if (!oscserveraddress)
        throw std::runtime_error("No \"oscserveraddress\" object in prefs.ui");

    ref_builder->get_widget("oscserverport", oscserverport);
    if (!oscserverport)
        throw std::runtime_error("No \"oscserverport\" object in prefs.ui");

    ref_builder->get_widget("oscupdaterate", oscupdaterate);
    if (!oscupdaterate)
        throw std::runtime_error("No \"oscupdaterate\" object in prefs.ui");

    settings = Gio::Settings::create("de.wavel.beziercomposer");
    settings->bind("guiplaybackfps", guiplaybackfps->property_active_id());
    settings->bind("enableosc", enableosc->property_active());
    settings->bind("oscserveraddress", oscserveraddress->property_text());
    settings->bind("oscserverport", oscserverport->property_value());
    settings->bind("oscupdaterate", oscupdaterate->property_active_id());
}

//static
ComposerPrefs* ComposerPrefs::create(Gtk::Window& parent)
{
// Load the Builder file and instantiate its widgets.
    auto ref_builder = Gtk::Builder::create_from_resource("/de/wavel/beziercomposer/prefs.ui");

    ComposerPrefs* dialog = nullptr;
    ref_builder->get_widget_derived("prefs_dialog", dialog);
    if (!dialog)
        throw std::runtime_error("No \"prefs_dialog\" object in prefs.ui");

    dialog->set_transient_for(parent);

    return dialog;
}
