/*
 * Copyright (C) 2021 Valentin Pratz <git@valentinpratz.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef BEZIERCURVE_H
#define BEZIERCURVE_H

#include "bezierpoint.h"
#include <list>
#include <tuple>

class BezierCurve
{
public:
    BezierCurve(long id);

    std::tuple<bool, double> get_value(double x);
    bool contains_gate(double start_time, double end_time);
    static std::tuple<double, double, double, double, double, double, double, double> calc_subdiv(double A, double B, double C, double D, double t);
    static double calc_bezier(double A, double B, double C, double D, double t);

    static std::tuple<bool, double> t_of_x(double x, BezierPoint& start_point, BezierPoint& end_point);
    static std::tuple<bool, double> y_of_x(double x, BezierPoint& start_point, BezierPoint& end_point);

    long id;
    std::list<BezierPoint*> points;
};
#endif // BEZIERCURVE_H
