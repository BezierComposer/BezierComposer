/*
 * Copyright (C) 2021 Valentin Pratz <git@valentinpratz.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "bezierpoint.h"
BezierPoint::BezierPoint(long id, double x, double y, double pre_rel_x, double pre_rel_y, double post_rel_x, double post_rel_y)
    : id(id), x(x), y(y), pre_rel_x(pre_rel_x), pre_rel_y(pre_rel_y), post_rel_x(post_rel_x), post_rel_y(post_rel_y)
{
};
