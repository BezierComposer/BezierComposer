/*
 * Copyright (C) 2021 Valentin Pratz <git@valentinpratz.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef BEZIERTRACK_H
#define BEZIERTRACK_H

#include "beziercurve.h"
#include "bezierpoint.h"
#include <jack/jack.h>
#include <list>
#include <tuple>

class BezierTrack {
public:
    enum Type { NOTES, CONTROL };
    BezierTrack(long id, jack_client_t* jack_client, bool register_ports=true);

    std::tuple<bool, double> get_value(double time);
    bool contains_gate(double start_time, double end_time);
    void register_jack_ports(bool use_sound_output=false, bool use_trig_output=false);
    void unregister_jack_ports();

    double to_track_x(double time, double units_per_beat, double time_offset);
    double to_track_y(double value);
    std::tuple<double, double> to_track_coords(double time, double value, double units_per_beat, double time_offset);
    double x_track_to_time(double x_track, double units_per_beat, double time_offset);
    double y_track_to_value(double y_track);
    std::tuple<double, double> to_time_value(double x_track, double y_track, double units_per_beat, double time_offset);
    std::tuple<BezierPoint::PointType, BezierPoint*, BezierCurve*, BezierTrack*> in_point(double x_track, double y_track, double units_per_beat, double time_offset, double handle_rad);

    static double midi_to_freq ( double d );
    static double freq_to_midi ( double freq );
    static std::tuple<int, int> freq_to_midi_pitch_bend ( double freq );

    long id;
    jack_client_t* jack_client = nullptr;

    std::list<BezierCurve*> curves;
    int channel;
    double last_freq = 0;
    int last_gate = 0;
    double volume;
    Type type;

    double height;
    double y_min;
    double y_max;


    jack_port_t* freq_snd_port = nullptr;
    jack_port_t* freq_cv_port = nullptr;
    jack_port_t* gate_cv_port = nullptr;
    jack_port_t* trig_cv_port = nullptr;
};
#endif // BEZIERTRACK_H
