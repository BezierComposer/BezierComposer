/*
 * Copyright (C) 2021 Valentin Pratz <git@valentinpratz.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef EDITOR_AREA_H
#define EDITOR_AREA_H

#include <gtkmm/drawingarea.h>
#include <list>
#include <jack/jack.h>
#include "bezierpoint.h"
#include "beziercurve.h"
#include "beziertrack.h"
#include "composition.h"
#include "pugixml.hpp"

class EditorArea : public Gtk::DrawingArea
{
public:
    EditorArea();
    virtual ~EditorArea();
    void set_composition(Composition* comp);
    void set_jack_client(jack_client_t* jack_client);
    void ctrl_add_point();
    void ctrl_remove_point();
    void ctrl_point_gate();
    void ctrl_add_curve();
    void ctrl_remove_curve();
    void ctrl_connect_points();
    void ctrl_disconnect_points();
    void ctrl_add_track();
    void ctrl_remove_track();
    void ctrl_copy();
    void ctrl_cut();
    void ctrl_paste();
    void ctrl_undo();
    void ctrl_redo();
    void set_snap(bool active);
    double get_playhead_pos();
    void set_playhead_pos(double pos);
    void add_to_selection(BezierPoint* p, BezierCurve* c, BezierTrack* t, bool reset=false);
    void reset_selection();
    void copy_selection(bool cut=false);

    enum DragType { DRAG_NONE, DRAG_POINT, DRAG_PLAYHEAD };
    enum SelectionType { SELECTION_NONE, SELECTION_SINGLE, SELECTION_MANY };

protected:
    bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;

    bool on_button_press_event(GdkEventButton * event);
    bool on_button_release_event(GdkEventButton * event);
    bool on_motion_notify_event(GdkEventMotion * event);
    bool on_scroll_event(GdkEventScroll * event);

private:
    void draw_vertical_grid(const Cairo::RefPtr<Cairo::Context>& cr);
    void draw_playhead(const Cairo::RefPtr<Cairo::Context>& cr);
    void draw_tracks(const Cairo::RefPtr<Cairo::Context>& cr);
    double draw_track(const Cairo::RefPtr<Cairo::Context>& cr, BezierTrack& track, double offset_x, double offset_y);
    void draw_curve(const Cairo::RefPtr<Cairo::Context>& cr, BezierCurve& curve, BezierTrack& track, double offset_x, double offset_y, bool background=false);
    void draw_track_grid(const Cairo::RefPtr<Cairo::Context>& cr, BezierTrack& track, double offset_x, double offset_y);
    void draw_bezier_point(const Cairo::RefPtr<Cairo::Context>& cr, BezierTrack& track, double offset_x, double offset_y, bool background=false);
    void draw_bezier_handles(const Cairo::RefPtr<Cairo::Context>& cr, BezierTrack& track, double offset_x, double offset_y);
    void draw_bezier_handle_axis(const Cairo::RefPtr<Cairo::Context>& cr, BezierTrack& track, double offset_x, double offset_y);
    void draw_bezier_gate(const Cairo::RefPtr<Cairo::Context>& cr, BezierTrack& track, double offset_x, double offset_y);
    void draw_bezier_segment(const Cairo::RefPtr<Cairo::Context>& cr, BezierTrack& track, BezierPoint& start_point, BezierPoint& end_point, double offset_x, double offset_y, bool lowres=false);


    int width, height;
    double track_offset_x;
    double track_offset_y;
    int scale;
    double handle_rad;
    double mouse_x, mouse_y;
    double playhead_height;
    double playhead_pos;
    double time_offset;
    double units_per_beat;
    int time_subdiv = 4;

    bool dragging;
    DragType drag_type;
    BezierPoint::PointType drag_point_type;
    BezierPoint* drag_point;
    BezierTrack* drag_track;
    bool drag_modified; // to see if history checkpoint has to be created

    std::list<BezierPoint*> selection_ptrs;
    std::list<BezierCurve*> selection_curve_ptrs;
    std::list<BezierTrack*> selection_track_ptrs;
    BezierTrack* selected_track_ptr = nullptr;

    pugi::xml_document copy_doc;

    bool snap = false;

    Composition* composition;
    jack_client_t *jack_client;

    std::tuple<BezierPoint::PointType, BezierPoint*, BezierCurve*, BezierTrack*> in_point(double x, double y);
    BezierTrack* in_track(double x, double y);
    double calc_bezier(double A, double B, double C, double D, double t);
};

#endif // EDITOR_AREA_H
