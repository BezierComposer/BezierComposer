/*
 * Copyright (C) 2021 Valentin Pratz <git@valentinpratz.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef COMPOSITION_H
#define COMPOSITION_H

#include <list>
#include <map>
#include <jack/jack.h>
#include "beziertrack.h"
#include "pugixml.hpp"

/*
Stores the musical data, used to initialize and deconstruct points, curves and tracks
*/
class Composition
{
public:
    Composition();

    BezierTrack* create_track(bool register_ports=true, long id=-1, int position=-1);
    void remove_track(long track_id, bool close_ports=true);
    BezierCurve* create_curve(long id=-1);
    void remove_curve(long curve_id);
    BezierPoint* create_point(long id=-1);
    BezierPoint* create_point(double x, double y, double pre_rel_x, double pre_rel_y, double post_rel_x, double post_rel_y, long id=-1);
    void remove_point(long point_id);

    // very inefficient undo/redo
    void clear_history();
    void store_history();
    bool undo();
    bool redo();

    void garbage_collect(unsigned int age=100); // probably unnecessary

    void from_xml(const pugi::xml_document& doc);
    pugi::xml_document to_xml();

    BezierPoint* add_point_at(double time, long curve_id);
    BezierPoint* add_point_at(double time, double value, long curve_id);
    BezierCurve* add_curve_at(double time, long track_id);
    BezierCurve* add_curve_at(double time, double value, long track_id);
    BezierPoint* subdivide_curve(double time, long curve_id);

    double get_length();


    std::list<BezierTrack*> tracks;
    std::map<long, BezierPoint*> pid; // id: point
    std::map<long, BezierCurve*> cid; // id: curve
    std::map<long, BezierTrack*> tid; // id: track
    jack_client_t* jack_client;
    double bpm;
    double sample_rate;
    bool loop;
private:
    void deconstruct_curve(long curve_id);
    void deconstruct_point(long point_id);

    long next_track_id();
    long next_curve_id();
    long next_point_id();

    std::list<BezierPoint*> deleted_points;
    std::list<BezierCurve*> deleted_curves;
    std::list<BezierTrack*> deleted_tracks;
    unsigned int garbage_counter;
    std::list<pugi::xml_document> history;
    int history_pos;
};
#endif // COMPOSITION_H
