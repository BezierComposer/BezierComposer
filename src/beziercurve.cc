/*
 * Copyright (C) 2021 Valentin Pratz <git@valentinpratz.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "beziercurve.h"
#include <math.h>


BezierCurve::BezierCurve(long id) : id(id), points()
{
};

std::tuple<bool, double> BezierCurve::get_value(double x)
{
    if (points.size() < 2)
        return std::make_tuple(false, 0.0);
    std::list<BezierPoint*>::iterator it = points.begin();
    BezierPoint* start_point = *it++;
    if (x < start_point->x )
        return std::make_tuple(false, 0.0);
    BezierPoint* end_point = nullptr;
    while (it != points.end())
    {
        end_point = *it++;
        if (x < end_point->x) {
            return y_of_x(x, *start_point, *end_point);
        }
        start_point = end_point;
    }
    return std::make_tuple(false, 0.0);
}

bool BezierCurve::contains_gate(double start_time, double end_time)
{
    std::list<BezierPoint*>::iterator it = points.begin();
    while (it != points.end())
    {
        if (start_time < (*it)->x && end_time > (*it)->x && (*it)->gate)
            return true;
        it++;
    }
    return false;
}

std::tuple<bool, double> BezierCurve::y_of_x(double x, BezierPoint& start_point, BezierPoint& end_point)
{
    bool found;
    double t;
    std::tie(found, t) = t_of_x(x, start_point, end_point);
    if (found) {
        double y = calc_bezier(start_point.y, start_point.y + start_point.post_rel_y,
                               end_point.y + end_point.pre_rel_y, end_point.y, t);
        return std::make_tuple(found, y);
    }
    return std::make_tuple(false, 0.0);
}


std::tuple<bool, double> BezierCurve::t_of_x(double x, BezierPoint& start_point, BezierPoint& end_point)
{
    // solving the bezier curve for t using https://de.wikipedia.org/wiki/Cardanische_Formeln
    double b0 = start_point.x;
    double b1 = start_point.x + start_point.post_rel_x;
    double b2 = end_point.x + end_point.pre_rel_x;
    double b3 = end_point.x;

    if (std::abs(x - b0) < 0.0001)
        return std::make_tuple(true, 0);
    if (std::abs(x - b3) < 0.0001)
        return std::make_tuple(true, 1);


    double A = -b0 + 3*b1 - 3*b2 + b3;
    double B = 3*b0 - 6*b1 + 3*b2;
    double C = -3*b0 + 3*b1;
    double D = b0 - x;

    // normalize
    if (A == 0)
        return std::make_tuple(false, 0.0);

    double p = (9*A*C-3*std::pow(B, 2))/(9*std::pow(A, 2));
    double q = (2*std::pow(B, 3) - 9*A*B*C+ 27*std::pow(A, 2)*D)/(27*std::pow(A, 3));

    double delta = std::pow((q/2), 2) + std::pow((p/3), 3);
    if (delta > 0) {
        double u = std::cbrt(-q/2+std::sqrt(delta));
        double v = std::cbrt(-q/2-std::sqrt(delta));

        double t = u + v - B/(3*A);
        return std::make_tuple(true, t);
    }
    if (delta == 0 && p == 0) {
        return std::make_tuple(true, -B/(3*A));
    }
    else if (delta == 0) {
        return std::make_tuple(true, 3*q/p-B/(3*A));
    }

    // casus irreducibilis delta < 0
    double ac = std::acos(-q/2*std::sqrt(-27/std::pow(p, 3)));
    double t2 = -std::sqrt(-4*p/3) * std::cos(ac/3 + M_PI/3) - B/(3*A);
    double t1 = std::sqrt(-4*p/3) * std::cos(ac/3) - B/(3*A);
    double t3 = -std::sqrt(-4*p/3) * std::cos(ac/3 - M_PI/3) - B/(3*A);
    bool found = false;
    // in case of multiple solutions in range, return the highest one
    double t;
    if (0 <= t1 && t1 <= 1) {
        t = t1;
        found = true;
    }
    if (0 <= t2 && t2 <= 1) {
        if (!found || t < t2)
            t = t2;
        found = true;
    }
    if (0 <= t3 && t3 <= 1) {
        if (!found || t < t3)
            t = t3;
        found = true;
    }
    if (found) {
        return std::make_tuple(true,t);
    }
    return std::make_tuple(false, 0.0);
}

std::tuple<double, double, double, double, double, double, double, double> BezierCurve::calc_subdiv(double A, double B, double C, double D, double t)
{
    // https://pomax.github.io/bezierinfo/#splitting
    double a0 = A;
    double b0 = t*B-(t-1)*A;
    double c0 = std::pow(t, 2)*C-2*t*(t-1)*B+std::pow(t-1, 2)*A;
    double d0 = std::pow(t, 3)*D-3*std::pow(t, 2)*(t-1)*C+3*t*std::pow(t-1, 2)*B-std::pow(t-1, 3)*A;
    double a1 = d0;
    double b1 = std::pow(t, 2)*D-2*t*(t-1)*C+std::pow(t-1, 2)*B;
    double c1 = t*D-(t-1)*C;
    double d1 = D;
    return std::make_tuple(a0, b0, c0, d0, a1, b1, c1, d1);
}

double BezierCurve::calc_bezier(double A, double B, double C, double D, double t)
{
    double s = 1 - t;
    double AB = A*s + B*t;
    double BC = B*s + C*t;
    double CD = C*s + D*t;
    double ABC = AB*s + BC*t;
    double BCD = BC*s + CD*t;
    return ABC*s + BCD*t;
}

