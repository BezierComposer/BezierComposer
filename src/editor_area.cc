/*
 * Copyright (C) 2021 Valentin Pratz <git@valentinpratz.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "editor_area.h"
#include <cairomm/context.h>
#include <iostream>
#include <cmath>
#include <string>
#include "bezierpoint.h"
#include "beziercurve.h"
#include "beziertrack.h"

EditorArea::EditorArea() :
           width(100), height(100),
           track_offset_x(100), track_offset_y(10),
           scale(1), handle_rad(4),
           mouse_x(0.0), mouse_y(0.0),
           playhead_height(10.0),
           playhead_pos(0.0),
           time_offset(0.0),
           units_per_beat(50.0),
           dragging(false),
           drag_type(DRAG_NONE),
           drag_point_type(BezierPoint::POINT_NONE),
           jack_client(nullptr)
{
    set_size_request (800, 600);
    add_events(Gdk::BUTTON_PRESS_MASK);
    add_events(Gdk::BUTTON_RELEASE_MASK);
    add_events(Gdk::POINTER_MOTION_MASK);
    add_events(Gdk::SCROLL_MASK);
}

EditorArea::~EditorArea()
{
}

void EditorArea::set_composition(Composition* comp)
{
    composition = comp;
}

void EditorArea::set_jack_client(jack_client_t* client)
{
    jack_client = client;
}


bool EditorArea::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    Gtk::Allocation allocation = get_allocation();
    width = allocation.get_width();
    height = allocation.get_height();

    cr->scale(1, 1);

    draw_vertical_grid(cr);
    draw_tracks(cr);
    draw_playhead(cr);
    return true;
}

bool EditorArea::on_button_press_event(GdkEventButton * event)
{
    if( (event->type == GDK_BUTTON_PRESS) && (event->button == 1) )
    {
        //the first coordinate

        if (event->y < track_offset_y && track_offset_x < event->x ) {
            dragging = true;
            drag_type = DRAG_PLAYHEAD;
            playhead_pos = (event->x - track_offset_x) / units_per_beat + time_offset;
            queue_draw();
        }
        else if (event->y > track_offset_y && track_offset_x < event->x) {
            BezierCurve *curve;
            BezierTrack* track = in_track(event->x, event->y);

            std::tie(drag_point_type, drag_point, curve, drag_track)  = in_point(event->x, event->y);
            if (drag_point_type != BezierPoint::POINT_NONE && drag_point && drag_track) {
                dragging = true;
                drag_modified = false;
                drag_type = DRAG_POINT;

                if ((event->state & Gdk::CONTROL_MASK) == Gdk::CONTROL_MASK) {
                    if (selection_ptrs.front() == drag_point) {
                        selection_ptrs.erase(selection_ptrs.begin());
                        selection_curve_ptrs.erase(selection_curve_ptrs.begin());
                        selection_track_ptrs.erase(selection_track_ptrs.begin());
                        dragging = false;
                    }
                    else
                        add_to_selection(drag_point, curve, drag_track);

                }
                else if ((event->state & Gdk::SHIFT_MASK) == Gdk::SHIFT_MASK) {
                    if (drag_track == selection_track_ptrs.front()) {
                        if (drag_point != selection_ptrs.front()) {
                            BezierPoint* start = selection_ptrs.front();
                            double min_x = std::min(drag_point->x, selection_ptrs.front()->x);
                            double max_x = std::max(drag_point->x, selection_ptrs.front()->x);
                            auto curve_it = drag_track->curves.begin();
                            while (curve_it != drag_track->curves.end()) {
                                auto point_it = (*curve_it)->points.begin();
                                while (point_it != (*curve_it)->points.end()) {
                                    if (min_x < (*point_it)->x && (*point_it)->x < max_x && (*point_it) != drag_point && (*point_it) != start)
                                        add_to_selection(*point_it, *curve_it, drag_track);
                                    point_it++;
                                }
                                curve_it++;
                            }
                            add_to_selection(drag_point, curve, drag_track);
                        }
                    }
                    else
                        add_to_selection(drag_point, curve, drag_track);
                }
                else {
                    if (std::find(selection_ptrs.begin(), selection_ptrs.end(), drag_point) == selection_ptrs.end())
                        add_to_selection(drag_point, curve, drag_track, true);
                    else
                        add_to_selection(drag_point, curve, drag_track);
                }
            }
            else {
                reset_selection();
            }
            selected_track_ptr = track;
            queue_draw();
        }
    }
    else if( (event->type == GDK_BUTTON_PRESS) && (event->button == 3) ) {
        if (event->y > track_offset_y && track_offset_x < event->x) {
            BezierTrack* track = in_track(event->x, event->y);
            double offset_y = track_offset_y;
            double offset_x = track_offset_x;
            std::list<BezierTrack*>::iterator track_it = composition->tracks.begin();
            while (track_it != composition->tracks.end())
            {
                if (event->y < offset_y + (*track_it)->height)
                    break;
                offset_y += (*track_it++)->height;
            }
            if (track) {
                double time = track->x_track_to_time(event->x - offset_x, units_per_beat, time_offset);
                double value = track->y_track_to_value(track->height - (event->y - offset_y));
                if (snap) {
                    time = std::round(time_subdiv*time)/time_subdiv;
                    value = std::round(value);
                }
                if ((event->state & Gdk::CONTROL_MASK) == Gdk::CONTROL_MASK) {
                    BezierCurve* curve = composition->add_curve_at(time, value, track->id);
                    reset_selection();
                    selected_track_ptr = track;
                    selection_track_ptrs.push_back(track);
                    selection_curve_ptrs.push_back(curve);
                    selection_ptrs.push_back(curve->points.front());
                    composition->store_history();
                }
                else if (!selection_curve_ptrs.empty() && selection_track_ptrs.front()->id == track->id) {
                    BezierCurve* curve = selection_curve_ptrs.front();
                    BezierPoint* p = composition->add_point_at(time, value, curve->id);
                    reset_selection();
                    selected_track_ptr = track;
                    selection_track_ptrs.push_back(track);
                    selection_curve_ptrs.push_back(curve);
                    selection_ptrs.push_back(p);
                    composition->store_history();
                }
                queue_draw();
            }
        }
    }
    return true;
}

bool EditorArea::on_button_release_event(GdkEventButton * event)
{
    if( (event->type == GDK_BUTTON_RELEASE) && (event->button == 1) )
    {
        if (drag_type == DRAG_PLAYHEAD)
        {
            if (track_offset_x > event->x)
                playhead_pos = time_offset;
            else if (event->x > width)
                playhead_pos = (width - track_offset_x) / units_per_beat + time_offset;
            else {
                if (snap)
                    playhead_pos = std::round(((event->x - track_offset_x) / units_per_beat)*time_subdiv)/time_subdiv + time_offset;
                else
                    playhead_pos = (event->x - track_offset_x) / units_per_beat + time_offset;
            }
            jack_transport_locate(jack_client, playhead_pos / (composition->bpm / 60) * composition->sample_rate);
            queue_draw();

        }
        else if (drag_type == DRAG_POINT && drag_modified)
            composition->store_history();
        drag_modified = false;
        dragging = false;
        drag_type = DRAG_NONE;
        drag_point_type = BezierPoint::POINT_NONE;
        drag_point = nullptr;
        drag_track = nullptr;
    }
    // The event has been handled.
    return true;
}

bool EditorArea::on_motion_notify_event(GdkEventMotion * event)
{
    mouse_x = event->x;
    mouse_y = event->y;
    switch ( drag_type ) {
    case DRAG_NONE:
        break;
    case DRAG_PLAYHEAD:
        if (track_offset_x < event->x && event->x < width) {
            if (snap)
                playhead_pos =std::round(((event->x - track_offset_x) / units_per_beat)*time_subdiv)/time_subdiv + time_offset;
            else
                playhead_pos = (event->x - track_offset_x) / units_per_beat + time_offset;
        }
        queue_draw();
        break;
    case DRAG_POINT:
        {
        if (drag_point && drag_track && dragging) {
            BezierPoint &p = *drag_point;
            double offset_y = track_offset_y;
            double offset_x = track_offset_x;
            std::list<BezierTrack*>::iterator track_it = composition->tracks.begin();
            BezierTrack* cur_track = nullptr;
            while (track_it != composition->tracks.end())
            {
                cur_track = *track_it++;
                offset_y += cur_track->height;
                if (mouse_y < offset_y) {
                    break;
                }
            }
            offset_y -= cur_track->height;
            if (cur_track == drag_track
                && offset_y < mouse_y && mouse_y < offset_y + cur_track->height
                && offset_x < mouse_x && mouse_x < width
                ) {
                switch ( drag_point_type ) {
                case BezierPoint::POINT_NONE:
                    break;
                case BezierPoint::POINT_MAIN:
                {
                    double x = cur_track->x_track_to_time(event->x - offset_x, units_per_beat, time_offset);
                    double y = cur_track->y_track_to_value(cur_track->height - (event->y - offset_y));
                    if (snap && !((event->state & Gdk::MOD1_MASK) == Gdk::MOD1_MASK))
                        y = std::round(y);
                    if (snap && !((event->state & Gdk::CONTROL_MASK) == Gdk::CONTROL_MASK))
                        x = std::round(x*time_subdiv)/time_subdiv;
                    double delta_x = x - p.x;
                    double delta_y = y - p.y;
                    // check if every point stays in composition
                    auto p_it = selection_ptrs.begin();
                    while (p_it != selection_ptrs.end()) {
                        if ((*p_it)->x + delta_x < 0)
                            delta_x = -(*p_it)->x;
                        if ((*p_it)->y + delta_y < 0)
                            delta_y = -(*p_it)->y;
                        p_it++;
                    }

                    p_it = selection_ptrs.begin();
                    while (p_it != selection_ptrs.end()) {
                        (*p_it)->x += delta_x;
                        (*p_it)->y += delta_y;
                        p_it++;
                    }
                    drag_modified = true;
                    queue_draw();
                    break;
                }
                case BezierPoint::POINT_HANDLE_PRE:
                    p.pre_rel_x = cur_track->x_track_to_time(event->x - offset_x, units_per_beat, time_offset) - p.x;
                    p.pre_rel_y = cur_track->y_track_to_value(cur_track->height - (event->y - offset_y)) - p.y;
                    if (snap && !((event->state & Gdk::MOD1_MASK) == Gdk::MOD1_MASK))
                        p.pre_rel_y = std::round(p.y + p.pre_rel_y) - p.y;
                    /* don't snap in x direction
                    if (snap && !((event->state & Gdk::CONTROL_MASK) == Gdk::CONTROL_MASK))
                        p.pre_rel_x = std::round((p.x + p.pre_rel_x)*time_subdiv)/time_subdiv - p.x;
                    */
                    drag_modified = true;
                    queue_draw();
                    break;
                case BezierPoint::POINT_HANDLE_POST:
                    p.post_rel_x = cur_track->x_track_to_time(event->x - offset_x, units_per_beat, time_offset) - p.x;
                    p.post_rel_y = cur_track->y_track_to_value(cur_track->height - (event->y - offset_y)) - p.y;
                    if (snap && !((event->state & Gdk::MOD1_MASK) == Gdk::MOD1_MASK))
                        p.post_rel_y = std::round(p.y + p.post_rel_y) - p.y;
                    /* don't snap in x direction
                    if (snap && !((event->state & Gdk::CONTROL_MASK) == Gdk::CONTROL_MASK))
                        p.post_rel_x = std::round((p.x + p.post_rel_x)*time_subdiv)/time_subdiv - p.x;
                    */
                    drag_modified = true;
                    queue_draw();
                    break;
                }
            }
        }
        }
    }

    return true;
}

bool EditorArea::on_scroll_event(GdkEventScroll *event)
{
    if (event->x > track_offset_x && event->x < width) {
        if ((event->state & Gdk::SHIFT_MASK) == Gdk::SHIFT_MASK && (event->state & Gdk::MOD1_MASK) == 0) {
            if (event->direction == GDK_SCROLL_UP)
                time_offset += 1;
            else {
                if (time_offset - 1 > 0)
                    time_offset -= 1;
                else
                    time_offset = 0;
            }
        }
        else if ((event->state & Gdk::CONTROL_MASK) == Gdk::CONTROL_MASK && (event->state & Gdk::MOD1_MASK) == 0) {
            if (event->direction == GDK_SCROLL_UP)
                units_per_beat *= 2;
            else if (units_per_beat > 1)
                units_per_beat /= 2;
        }
        else if ((event->state & Gdk::CONTROL_MASK) == Gdk::CONTROL_MASK && (event->state & Gdk::MOD1_MASK) == Gdk::MOD1_MASK) {
            BezierTrack* track = in_track(event->x, event->y);
            if (track) {
                double zoom_midi = (track->y_max - track->y_min) / 10;
                if (event->direction == GDK_SCROLL_UP) {
                    track->y_min = track->y_min + zoom_midi;
                    track->y_max = track->y_max - zoom_midi;
                }
                else if ((track->y_max - track->y_min) < 80) {
                    track->y_min = track->y_min - zoom_midi;
                    track->y_max = track->y_max + zoom_midi;
                }
                if (track->y_min < 0)
                    track->y_min = 0;
            }
        }
        else {
            BezierTrack* track = in_track(event->x, event->y);
            if (track) {
                double scroll_midi = (track->y_max - track->y_min) / 10;
                if (event->direction == GDK_SCROLL_UP) {
                    track->y_min = track->y_min + scroll_midi;
                    track->y_max = track->y_max + scroll_midi;
                }
                else {
                    if (track->freq_to_midi(track->y_min) - scroll_midi < 0)
                        scroll_midi = track->y_min;
                    track->y_min = track->y_min - scroll_midi;
                    track->y_max = track->y_max - scroll_midi;

                }
            }
        }
    }
    else if (event->x < track_offset_x) {
        double offset_y = track_offset_y;
        std::list<BezierTrack*>::iterator track_it = composition->tracks.begin();
        while (track_it != composition->tracks.end())
        {
            if (event->y < offset_y + (*track_it)->height) {
                BezierTrack* track = *track_it;
                double zoom_height = track->height / 10;
                if (event->direction == GDK_SCROLL_UP) {
                    track->height += zoom_height;
                }
                else {
                    track->height -= zoom_height;
                    if (track->height < 40)
                        track->height = 40;
                }
                break;
            }
            offset_y += (*track_it++)->height;
        }
    }
    queue_draw();
    return true;
}

void EditorArea::add_to_selection(BezierPoint* p, BezierCurve* c, BezierTrack* t, bool reset)
{
    if (reset) {
        selection_ptrs.clear();
        selection_curve_ptrs.clear();
        selection_track_ptrs.clear();
    }
    auto it = std::find(selection_ptrs.begin(), selection_ptrs.end(), p);
    if (it != selection_ptrs.end()) {
        int index = std::distance(selection_ptrs.begin(), it);
        selection_ptrs.erase(it);
        auto curve_it = selection_curve_ptrs.begin();
        auto track_it = selection_track_ptrs.begin();
        std::advance(curve_it, index);
        std::advance(track_it, index);
        selection_curve_ptrs.erase(curve_it);
        selection_track_ptrs.erase(track_it);
    }
    selection_ptrs.insert(selection_ptrs.begin(), p);
    selection_curve_ptrs.insert(selection_curve_ptrs.begin(), c);
    selection_track_ptrs.insert(selection_track_ptrs.begin(), t);
}

void EditorArea::reset_selection()
{
    selection_ptrs.clear();
    selection_curve_ptrs.clear();
    selection_track_ptrs.clear();

    dragging = false;
    drag_type = DRAG_NONE;
    drag_point_type = BezierPoint::POINT_NONE;
    drag_point = nullptr;
    drag_track = nullptr;
}

void EditorArea::copy_selection(bool cut)
{
    if (selection_ptrs.empty())
        return;
    copy_doc.reset();
    pugi::xml_node comp = copy_doc.append_child("copy");

    // brute force, could be done smarter
    int first_track_pos = composition->tracks.size();
    double min_x = 100000;

    std::list<BezierTrack*>::iterator track_it = composition->tracks.begin();
    int pos = 0;
    while (track_it != composition->tracks.end()) {
        if (std::find(selection_track_ptrs.begin(), selection_track_ptrs.end(), *track_it) == selection_track_ptrs.end()) {
            // no point in track selected
            track_it++;
            pos++;
            continue;
        }
        if (pos < first_track_pos)
            first_track_pos = pos;
        BezierTrack tr = **track_it++;
        pugi::xml_node track_node = comp.append_child("track");
        track_node.append_attribute("pos").set_value(pos);
        track_node.append_attribute("type").set_value((int) tr.type);

        std::list<BezierCurve*>::iterator curve_it = tr.curves.begin();
        while (curve_it != tr.curves.end()) {
            if (std::find(selection_curve_ptrs.begin(), selection_curve_ptrs.end(), *curve_it) == selection_curve_ptrs.end()) {
                curve_it++;
                continue;
            }
            BezierCurve curve = **curve_it++;
            pugi::xml_node curve_node = track_node.append_child("curve");

            std::list<BezierPoint*>::iterator points_it = curve.points.begin();
            while (points_it != curve.points.end()) {
                if (std::find(selection_ptrs.begin(), selection_ptrs.end(), *points_it) == selection_ptrs.end()) {
                    points_it++;
                    continue;
                }
                BezierPoint &p = **points_it++;
                if (p.x < min_x)
                    min_x = p.x;
                pugi::xml_node point_node = curve_node.append_child("point");
                point_node.append_attribute("x").set_value(p.x);
                point_node.append_attribute("y").set_value(p.y);
                point_node.append_attribute("pre_rel_x").set_value(p.pre_rel_x);
                point_node.append_attribute("pre_rel_y").set_value(p.pre_rel_y);
                point_node.append_attribute("post_rel_x").set_value(p.post_rel_x);
                point_node.append_attribute("post_rel_y").set_value(p.post_rel_y);
                point_node.append_attribute("gate").set_value(p.gate);
            }
        }
        pos++;
    }
    comp.append_attribute("min_x").set_value(min_x);
    comp.append_attribute("min_track").set_value(first_track_pos);
    if (cut) {
        ctrl_remove_point();
        queue_draw();
    }
}

void EditorArea::ctrl_paste()
{
    // copy_doc.save(std::cout);
    pugi::xml_node comp = copy_doc.child("copy");
    if (comp.type() == pugi::node_null || !selected_track_ptr) {
        return;
    }
    int min_track = comp.attribute("min_track").as_int();
    double min_x = comp.attribute("min_x").as_double();

    auto selected_track_it = std::find(composition->tracks.begin(), composition->tracks.end(), selected_track_ptr);
    int selected_pos = std::distance(composition->tracks.begin(), selected_track_it);

    selection_ptrs.clear();
    selection_curve_ptrs.clear();
    selection_track_ptrs.clear();

    for (pugi::xml_node track_node = comp.child("track"); track_node; track_node = track_node.next_sibling("track")) {
        int cur_pos = selected_pos + (track_node.attribute("pos").as_int() - min_track);
        if (cur_pos >= (int) composition->tracks.size())
            continue;
        auto cur_track_it = composition->tracks.begin();
        std::advance(cur_track_it, cur_pos);
        BezierTrack &track = **cur_track_it;

        for (pugi::xml_node curve_node = track_node.child("curve"); curve_node; curve_node = curve_node.next_sibling("curve")) {
            BezierCurve* curve = composition->create_curve();
            for (pugi::xml_node p_node = curve_node.child("point"); p_node; p_node = p_node.next_sibling("point")) {
                BezierPoint* p = composition->create_point(
                    p_node.attribute("x").as_double() - min_x + playhead_pos,
                    p_node.attribute("y").as_double(),
                    p_node.attribute("pre_rel_x").as_double(),
                    p_node.attribute("pre_rel_y").as_double(),
                    p_node.attribute("post_rel_x").as_double(),
                    p_node.attribute("post_rel_y").as_double()
                );
                p->gate = p_node.attribute("gate").as_bool();
                curve->points.push_back(p);
                selection_ptrs.insert(selection_ptrs.begin(), p);
                selection_curve_ptrs.insert(selection_curve_ptrs.begin(), curve);
                selection_track_ptrs.insert(selection_track_ptrs.begin(), &track);

            }
            track.curves.push_back(curve);
        }
    }
    composition->store_history();
    queue_draw();
}

void EditorArea::ctrl_undo()
{
    if (composition->undo()) {
        reset_selection();
        selected_track_ptr = nullptr;
        queue_draw();
    }
}

void EditorArea::ctrl_redo()
{
    if (composition->redo()) {
        reset_selection();
        selected_track_ptr = nullptr;
        queue_draw();
    }
}

void EditorArea::draw_vertical_grid(const Cairo::RefPtr<Cairo::Context>& cr)
{
    double font_size = 12.0;
    double min_units = 15.0;
    // TODO implement time_offset
    cr->set_line_width(1);
    cr->set_source_rgba(0.0, 0.0, 0.0, 0.6);
    cr->select_font_face("Purisa",
                        Cairo::FONT_SLANT_NORMAL,
                        Cairo::FONT_WEIGHT_BOLD);
    cr->set_font_size(font_size);
    std::stringstream name;
    int start_pos = std::ceil(time_offset);
    for (unsigned int pos = start_pos; (pos - time_offset) * units_per_beat < width - track_offset_x; pos++) {
        if (units_per_beat < min_units && pos % 4 != 0)
            continue;
        if (pos % 4 == 0) {
            name << pos / 4 + 1;
            if (units_per_beat > min_units)
                 name << "|" << pos % 4 + 1;
            cr->set_source_rgba(0.0, 0.0, 0.0, 0.9);
        }
        else {
            name << pos % 4 + 1;
            cr->set_source_rgba(0.0, 0.0, 0.0, 0.6);
        }
        cr->move_to(track_offset_x + (pos - time_offset) * units_per_beat, track_offset_y - 10);
        cr->line_to(track_offset_x + (pos - time_offset) * units_per_beat, height);
        cr->stroke();
        if (units_per_beat > 5) {
            cr->move_to(track_offset_x + (pos - time_offset) * units_per_beat + 2, track_offset_y-2);
            cr->show_text(name.str());
        }
        name.str("");
        name.clear();
    }
}

void EditorArea::draw_playhead(const Cairo::RefPtr<Cairo::Context>& cr)
{
    const double playhead_width = 16;
    double pos = (playhead_pos - time_offset) * units_per_beat;
    if (pos < 0 || track_offset_x + pos > width)
        return;
    cr->set_line_width(1);
    cr->set_source_rgba(1.0, 0.0, 0.0, 0.8);
    cr->move_to(track_offset_x + pos, track_offset_y - 10 + playhead_height);
    cr->line_to(track_offset_x + pos, height);
    cr->stroke();
    cr->set_line_width(1);
    cr->move_to(track_offset_x + pos - playhead_width/2, track_offset_y - 10);
    cr->line_to(track_offset_x + pos + playhead_width/2, track_offset_y - 10);
    cr->line_to(track_offset_x + pos, track_offset_y - 10 + playhead_height);
    cr->close_path();
    cr->fill();
}

void EditorArea::draw_tracks(const Cairo::RefPtr<Cairo::Context>& cr)
{
    double offset_x = track_offset_x;
    double offset_y = track_offset_y;
    // draw borders
    cr->set_line_width(2);
    cr->set_source_rgb(0.0, 0.0, 0.0);
    cr->move_to(0, offset_y);
    cr->line_to(width, offset_y);
    cr->move_to(offset_x, offset_y);
    cr->line_to(offset_x, height);
    cr->move_to(width, offset_y);
    cr->line_to(width, height);
    cr->stroke();

    std::list<BezierTrack*>::iterator it = composition->tracks.begin();
    while (it != composition->tracks.end() && offset_y < height)
    {
        offset_y += draw_track(cr, **it, offset_x, offset_y);
        // draw seperator
        cr->set_line_width(2);
        cr->set_source_rgb(0.0, 0.0, 0.0);
        cr->move_to(0, offset_y);
        cr->line_to(width, offset_y);
        cr->stroke();

        it++;
    }
}

double EditorArea::draw_track(const Cairo::RefPtr<Cairo::Context>& cr, BezierTrack& track, double offset_x, double offset_y)
{
    cr->rectangle(0, offset_y, width, track.height);
    cr->clip();
    draw_track_grid(cr, track, offset_x, offset_y);
    cr->reset_clip();

    cr->rectangle(offset_x, offset_y, width - offset_x, track.height);
    cr->clip();

    // trace other tracks

    auto other_it = composition->tracks.begin();
    while (other_it != composition->tracks.end()) {
        if ((*other_it) != &track && !(*other_it)->curves.empty()) {
            auto other_curve_it = (*other_it)->curves.begin();
            while (other_curve_it != (*other_it)->curves.end()) {
                draw_curve(cr, **other_curve_it++, track, offset_x, offset_y, true);
            }
        }
        other_it++;
    }

    if (track.curves.size() == 0) {
        cr->reset_clip();
        return track.height;
    }

    draw_bezier_handle_axis(cr, track, offset_x, offset_y);

    std::list<BezierCurve*>::iterator c_it = track.curves.begin();
    while (c_it != track.curves.end()) {
        draw_curve(cr, **c_it++, track, offset_x, offset_y);
    }

    draw_bezier_handles(cr, track, offset_x, offset_y);
    draw_bezier_gate(cr, track, offset_x, offset_y);
    draw_bezier_point(cr, track, offset_x, offset_y);

    cr->reset_clip();
    return track.height;
}

void EditorArea::draw_bezier_handles(const Cairo::RefPtr<Cairo::Context>& cr, BezierTrack& track, double offset_x, double offset_y)
{
    const double HANDLE_RAD = 4;
    auto c_it = track.curves.begin();
    while (c_it != track.curves.end()) {
        std::list<BezierPoint*>::iterator it = (*c_it)->points.begin();

        while(it != (*c_it)->points.end())
        {
            BezierPoint* point = *it++;
            double pre_x, pre_y, post_x, post_y;
            std::tie(pre_x, pre_y) = track.to_track_coords(point->x + point->pre_rel_x, point->y + point->pre_rel_y, units_per_beat, time_offset);
            std::tie(post_x, post_y) = track.to_track_coords(point->x + point->post_rel_x, point->y + point->post_rel_y, units_per_beat, time_offset);

            cr->move_to(offset_x + pre_x + HANDLE_RAD / 2, offset_y + track.height - pre_y);
            cr->arc(offset_x + pre_x, offset_y + track.height - pre_y, HANDLE_RAD / 2, 0.0, 2.0 * M_PI);
            cr->move_to(offset_x + post_x - HANDLE_RAD / 2, offset_y + track.height - post_y);
            cr->arc(offset_x + post_x, offset_y + track.height - post_y, HANDLE_RAD / 2, 0.0, 2.0 * M_PI);
        }
        c_it++;
    }
    cr->set_line_width(HANDLE_RAD);
    cr->set_source_rgba(0.0, 0.0, 0.5, 0.7);
    cr->stroke();
}

void EditorArea::draw_bezier_handle_axis(const Cairo::RefPtr<Cairo::Context>& cr, BezierTrack& track, double offset_x, double offset_y)
{
    auto c_it = track.curves.begin();
    while (c_it != track.curves.end()) {
        std::list<BezierPoint*>::iterator it = (*c_it)->points.begin();

        while(it != (*c_it)->points.end())
        {
            BezierPoint* point = *it++;
            double x, y, pre_x, pre_y, post_x, post_y;
            std::tie(x, y) = track.to_track_coords(point->x, point->y, units_per_beat, time_offset);
            std::tie(pre_x, pre_y) = track.to_track_coords(point->x + point->pre_rel_x, point->y + point->pre_rel_y, units_per_beat, time_offset);
            std::tie(post_x, post_y) = track.to_track_coords(point->x + point->post_rel_x, point->y + point->post_rel_y, units_per_beat, time_offset);

            cr->move_to(offset_x + x, offset_y + track.height - y);
            cr->line_to(offset_x + pre_x, offset_y + track.height - pre_y);
            cr->move_to(offset_x + x, offset_y + track.height - y);
            cr->line_to(offset_x + post_x, offset_y + track.height - post_y);
        }
        c_it++;
    }
    cr->set_line_width(1);
    cr->set_source_rgba(0.6, 0.2, 0.2, 0.6);
    cr->stroke();
}

void EditorArea::draw_bezier_gate(const Cairo::RefPtr<Cairo::Context>& cr, BezierTrack& track, double offset_x, double offset_y)
{
    auto c_it = track.curves.begin();
    while (c_it != track.curves.end()) {
        std::list<BezierPoint*>::iterator it = (*c_it)->points.begin();

        while(it != (*c_it)->points.end())
        {
            BezierPoint* point = *it++;
            if (point->gate) {
                double x, y;
                std::tie(x, y) = track.to_track_coords(point->x, point->y, units_per_beat, time_offset);
                double gate_height = 10;
                cr->move_to(offset_x + x, offset_y + track.height - y - gate_height);
                cr->line_to(offset_x + x, offset_y + track.height - y + gate_height);
            }
        }
        c_it++;
    }
    cr->set_line_width(3);
    cr->set_source_rgba(0.0, 0.0, 0.2, 0.7);
    cr->stroke();
}

void EditorArea::draw_bezier_point(const Cairo::RefPtr<Cairo::Context>& cr, BezierTrack& track, double offset_x, double offset_y, bool background) {
    const double HANDLE_RAD = 4;
    cr->set_line_width(HANDLE_RAD);

    BezierPoint* sel = nullptr;
    std::list<BezierPoint*> selected;

    auto c_it = track.curves.begin();
    while (c_it != track.curves.end()) {
        std::list<BezierPoint*>::iterator it = (*c_it)->points.begin();

        while(it != (*c_it)->points.end())
        {
            BezierPoint* point = *it++;
            double x, y;
            std::tie(x, y) = track.to_track_coords(point->x, point->y, units_per_beat, time_offset);
            if (x < 0 || x > width - offset_x || y < 0 || y > track.height) // not visible
                continue;
            if (selection_ptrs.front() == point) {
                sel = point;
            }
            else if (std::find(selection_ptrs.begin(), selection_ptrs.end(), point) != selection_ptrs.end()){
                selected.push_back(point);
            }
            else
                cr->rectangle(offset_x + x - HANDLE_RAD / 2, offset_y + track.height - y - HANDLE_RAD / 2, HANDLE_RAD , HANDLE_RAD);
        }
        c_it++;
    }
    cr->set_source_rgba(0.0, 0.7, 0.0, 0.7);
    cr->stroke();

    if (sel) {
        cr->set_source_rgba(0.0, 0.9, 0.9, 0.7);
        double x, y;
        std::tie(x, y) = track.to_track_coords(sel->x, sel->y, units_per_beat, time_offset);
        cr->rectangle(offset_x + x - HANDLE_RAD / 2, offset_y + track.height - y - HANDLE_RAD / 2, HANDLE_RAD , HANDLE_RAD);
        cr->stroke();
    }

    auto sel_it = selected.begin();
    while (sel_it != selected.end()) {
        double x, y;
        std::tie(x, y) = track.to_track_coords((*sel_it)->x, (*sel_it)->y, units_per_beat, time_offset);
        cr->rectangle(offset_x + x - HANDLE_RAD / 2, offset_y + track.height - y - HANDLE_RAD / 2, HANDLE_RAD , HANDLE_RAD);
        sel_it++;
    }
    cr->set_source_rgba(0.0, 0.6, 0.6, 0.7);
    cr->stroke();

    c_it = track.curves.begin();
    while (c_it != track.curves.end()) {
        std::list<BezierPoint*>::iterator it = (*c_it)->points.begin();

        while(it != (*c_it)->points.end())
        {
            BezierPoint* point = *it++;
            double x, y;
            std::tie(x, y) = track.to_track_coords(point->x, point->y, units_per_beat, time_offset);
            if (x < 0 || x > width - offset_x || y < 0 || y > track.height) // not visible
                continue;
            cr->rectangle(offset_x + x - HANDLE_RAD, offset_y + track.height - y - HANDLE_RAD, 2 * HANDLE_RAD, 2 * HANDLE_RAD);
        }
        c_it++;
    }
    cr->set_line_width(1);
    cr->set_source_rgba(0.0, 0.0, 0.0, 0.4);
    cr->stroke();
}

void EditorArea::draw_curve(const Cairo::RefPtr<Cairo::Context>& cr, BezierCurve& curve, BezierTrack& track, double offset_x, double offset_y, bool background)
{
    std::list<BezierPoint*> &points = curve.points;
    std::list<BezierPoint*>::iterator it = points.begin();

    cr->set_source_rgb(0.0, 0.0, 0.0);

    it = points.begin();
    BezierPoint* start_point = *it++;
    BezierPoint* end_point;
    if (background) {
        cr->set_source_rgba(0.2, 0.2, 0.8, 0.4);
    }
    else {
        if (&curve == selection_curve_ptrs.front())
            cr->set_source_rgba(0.0, 0.6, 0.0, 1.0);
        else if (std::find(selection_curve_ptrs.begin(), selection_curve_ptrs.end(), &curve) != selection_curve_ptrs.end())
            cr->set_source_rgba(0.0, 0.3, 0.0, 1.0);
        else
            cr->set_source_rgba(0.0, 0.0, 0.0, 1.0);
    }
    while(it != points.end())
    {
        end_point = *it++;
        draw_bezier_segment(cr, track, *start_point, *end_point, offset_x, offset_y, background);
        start_point = end_point;
    }

    cr->stroke();
}

void EditorArea::draw_track_grid(const Cairo::RefPtr<Cairo::Context>& cr, BezierTrack& track, double offset_x, double offset_y)
{
    double keyboard_width = 30;
    double font_size = 12;
    cr->set_line_width(1);
    double line_dist = track.to_track_y(1) - track.to_track_y(0);
    int start_pos = std::floor(track.y_min);
    if (start_pos < 0)
        start_pos = 0;
    std::string note_names[12] = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
    std::stringstream name;
    for (unsigned int pos = start_pos; track.to_track_y(pos) < track.height + line_dist; pos++) {
        double y = offset_y + track.height - track.to_track_y(pos);
        if (selected_track_ptr == &track)
            cr->set_source_rgba(0.4, 0.0, 0.1, 0.6);
        else
            cr->set_source_rgba(0.0, 0.0, 0.0, 0.6);
        cr->move_to(offset_x, y);
        cr->line_to(width, y);
        cr->stroke();
        cr->save();
        cr->rectangle(offset_x - keyboard_width, y - line_dist / 2, keyboard_width, line_dist);
        cr->set_source_rgba(0.0, 0.0, 0.0, 1.0);

        if ((pos % 12) == 1 || (pos % 12) == 3 || (pos % 12) == 6 || (pos % 12) == 8 || (pos % 12) == 10) {
            cr->fill_preserve();
            cr->stroke();
            cr->set_source_rgba(1.0, 1.0, 1.0, 1.0);
        }
        else {
            cr->stroke();
            cr->set_source_rgba(0.0, 0.0, 0.0, 1.0);
        }
        cr->select_font_face("Purisa",
                            Cairo::FONT_SLANT_NORMAL,
                            Cairo::FONT_WEIGHT_BOLD);
        cr->set_font_size(font_size);
        cr->move_to(offset_x - 27, y + font_size / 2);
        name << note_names[pos%12] << (pos - (pos%12)) / 12;
        cr->show_text(name.str());
        name.str("");
        name.clear();
        cr->restore();
    }
}

void EditorArea::draw_bezier_segment(const Cairo::RefPtr<Cairo::Context>& cr, BezierTrack& track, BezierPoint& start_point, BezierPoint& end_point, double offset_x, double offset_y, bool lowres)
{
    double start_x = track.to_track_x(start_point.x, units_per_beat, time_offset);
    double end_x = track.to_track_x(end_point.x, units_per_beat, time_offset);
    if (end_x < 0 || start_x > width - offset_x) // not visible
        return;
    double x0, y0, x1, y1, x2, y2, x3, y3;
    std::tie(x0, y0) = track.to_track_coords(start_point.x, start_point.y, units_per_beat, time_offset);
    std::tie(x1, y1) = track.to_track_coords(start_point.x + start_point.post_rel_x, start_point.y + start_point.post_rel_y, units_per_beat, time_offset);
    std::tie(x2, y2) = track.to_track_coords(end_point.x + end_point.pre_rel_x, end_point.y + end_point.pre_rel_y, units_per_beat, time_offset);
    std::tie(x3, y3) = track.to_track_coords(end_point.x, end_point.y, units_per_beat, time_offset);
    cr->move_to(offset_x + x0, offset_y + track.height - y0);
    cr->curve_to(offset_x + x1, offset_y + track.height - y1, offset_x + x2, offset_y + track.height - y2, offset_x + x3, offset_y + track.height - y3);
    cr->stroke();

    if (start_point.x < playhead_pos && end_point.x > playhead_pos) {
        bool valid;
        double value;
        std::tie(valid, value) = BezierCurve::y_of_x(playhead_pos, start_point, end_point);
        double x, y;
        std::tie(x, y) = track.to_track_coords(playhead_pos, value, units_per_beat, time_offset);
        cr->save();
        cr->move_to(offset_x + x, offset_y + track.height - y);
        cr->arc(offset_x + x, offset_y + track.height - y, 2, 0.0, 2.0 * M_PI);
        cr->set_source_rgba(0.0, 0.0, 0.7, 0.9);
        cr->fill();
        cr->restore();
    }
}

std::tuple<BezierPoint::PointType, BezierPoint*, BezierCurve*, BezierTrack*> EditorArea::in_point(double x, double y) {
    double offset_x = track_offset_x;
    double offset_y = track_offset_y;

    std::list<BezierTrack*>::iterator track_it = composition->tracks.begin();
    BezierPoint::PointType type;
    BezierPoint* point;
    BezierCurve* curve;
    BezierTrack* todo_delete;
    while (track_it != composition->tracks.end())
    {
        BezierTrack &track = **track_it++;

        if (offset_y < y && y < offset_y + track.height && offset_x < x) {
            std::tie(type, point, curve, todo_delete) = track.in_point(x - offset_x, track.height - (y - offset_y), units_per_beat, time_offset, handle_rad);
            if (type != BezierPoint::POINT_NONE) {
                return std::make_tuple(type, point, curve, &track);
            }
        }
        offset_y += track.height;
    }
    return std::make_tuple(BezierPoint::POINT_NONE, nullptr, nullptr, nullptr);
}

BezierTrack* EditorArea::in_track(double x, double y) {
    double offset_x = track_offset_x;
    double offset_y = track_offset_y;

    std::list<BezierTrack*>::iterator track_it = composition->tracks.begin();
    while (track_it != composition->tracks.end())
    {
        BezierTrack &track = **track_it++;

        if (offset_y < y && y < offset_y + track.height && offset_x < x) {
            return &track;
        }
        offset_y += track.height;
    }
    return nullptr;
}

double EditorArea::calc_bezier(double A, double B, double C, double D, double t)
{
    double s = 1 - t;
    double AB = A*s + B*t;
    double BC = B*s + C*t;
    double CD = C*s + D*t;
    double ABC = AB*s + BC*t;
    double BCD = BC*s + CD*t;
    return ABC*s + BCD*t;
}

void EditorArea::ctrl_add_point()
{
    if (selection_curve_ptrs.size() > 0 && selection_curve_ptrs.front()) {
        BezierCurve* cur_curve = selection_curve_ptrs.front();
        BezierTrack* cur_track = selection_track_ptrs.front();
        BezierPoint* p = composition->add_point_at(playhead_pos, cur_curve->id);
        if (p) {
            reset_selection();
            selection_track_ptrs.push_back(cur_track);
            selection_curve_ptrs.push_back(cur_curve);
            selection_ptrs.push_back(p);
            queue_draw();
            composition->store_history();
        }
    }
}

void EditorArea::ctrl_remove_point()
{
    if (!selection_ptrs.empty()) {
        auto it = selection_ptrs.begin();
        while (it != selection_ptrs.end()) {
            composition->remove_point((*it++)->id);
        }
        reset_selection();
        queue_draw();
        composition->store_history();
    }
}

void EditorArea::ctrl_point_gate()
{
    if (selection_ptrs.empty())
        return;
    std::list<BezierPoint*>::iterator it = selection_ptrs.begin();
    while (it != selection_ptrs.end()) {
        (*it)->gate = !((*it)->gate);
        it++;
    }
    queue_draw();
    composition->store_history();
}

void EditorArea::ctrl_add_curve()
{
    if (selected_track_ptr) {
        BezierTrack* cur_track = selected_track_ptr;

        BezierCurve* curve = composition->add_curve_at(playhead_pos, cur_track->id);
        reset_selection();
        selection_track_ptrs.push_back(cur_track);
        selection_curve_ptrs.push_back(curve);
        selection_ptrs.push_back(curve->points.front());
        queue_draw();
        composition->store_history();
    }
}

void EditorArea::ctrl_remove_curve()
{
    if (!selection_curve_ptrs.empty()) {
        auto it = selection_curve_ptrs.begin();
        while (it != selection_curve_ptrs.end()) {
            composition->remove_curve((*it++)->id);
        }
        reset_selection();
        queue_draw();
        composition->store_history();
    }
}

void EditorArea::ctrl_connect_points()
{
    if (selection_track_ptrs.size() < 2)
        return;
    auto t_it = selection_track_ptrs.begin();
    BezierTrack* t1 = *t_it++;
    BezierTrack* t2 = *t_it;
    if (t1 != t2)
        return;
    auto c_it = selection_curve_ptrs.begin();
    BezierCurve* c1 = *c_it++;
    BezierCurve* c2 = *c_it;
    if (c1 == c2)
        return;
    auto p_it = selection_ptrs.begin();
    BezierPoint* p1 = *p_it++;
    BezierPoint* p2 = *p_it;
    if (p1 == c1->points.front() && p2 == c2->points.back())
    {
        c2->points.splice(c2->points.end(), c1->points);
        composition->remove_curve(c1->id);
        reset_selection();
        queue_draw();
        composition->store_history();
    }
    else if (p2 == c2->points.front() && p1 == c1->points.back()) {
        c1->points.splice(c1->points.end(), c2->points);
        composition->remove_curve(c2->id);
        reset_selection();
        queue_draw();
        composition->store_history();
    }
}

void EditorArea::ctrl_disconnect_points()
{
    if (selection_track_ptrs.size() < 2)
        return;
    auto t_it = selection_track_ptrs.begin();
    BezierTrack* t = *t_it++;
    if (t != (*t_it))
        return;
    auto c_it = selection_curve_ptrs.begin();
    BezierCurve* c = *c_it++;
    if (c != (*c_it))
        return;
    auto p_sel_it = selection_ptrs.begin();
    BezierPoint* p1 = *p_sel_it++;
    BezierPoint* p2 = *p_sel_it;

    BezierPoint* cur = nullptr;
    auto p_it = c->points.begin();
    // check if they are neighbours and move seperator to second point
    while (p_it != c->points.end())
    {
        cur = *p_it++;
        if (p1 == cur) {
            if (p2 == (*p_it))
                break;
            else
                return;
        }
        else if (p2 == cur) {
            if (p1 == (*p_it))
                break;
            else
                return;
        }
    }

    BezierCurve* new_curve = composition->create_curve();
    new_curve->points.splice(new_curve->points.begin(), c->points, p_it, c->points.end());
    t->curves.push_back(new_curve);
    reset_selection();
    queue_draw();
    composition->store_history();
}

void EditorArea::ctrl_add_track()
{
    BezierTrack* track = composition->create_track();
    composition->tracks.push_back(track);
    selected_track_ptr = track;
    queue_draw();
    composition->store_history();
}

void EditorArea::ctrl_remove_track()
{
    if (selected_track_ptr) {
        composition->remove_track(selected_track_ptr->id);
        reset_selection();
        selected_track_ptr = nullptr;
        queue_draw();
        composition->store_history();
    }
}

void EditorArea::ctrl_copy()
{
    copy_selection(false);
}

void EditorArea::ctrl_cut()
{
    copy_selection(true);
    composition->store_history();
}

void EditorArea::set_snap(bool active)
{
    snap = active;
}

void EditorArea::set_playhead_pos(double beat)
{
    if (!(dragging && drag_type == DRAG_PLAYHEAD)) {
        playhead_pos = beat;
        queue_draw();
    }
}

double EditorArea::get_playhead_pos()
{
    return playhead_pos;
}
