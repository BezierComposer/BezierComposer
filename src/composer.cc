/*
 * Copyright (C) 2021 Valentin Pratz <git@valentinpratz.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "composer.h"
#include "oscpack/osc/OscOutboundPacketStream.h"
#include "oscpack/ip/UdpSocket.h"
#include <iostream>
#include <sstream>
#include <string>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/filefilter.h>
// using namespace stk;

BezierComposer::BezierComposer(const Glib::RefPtr<Gtk::Application>& app) : vbox( Gtk::ORIENTATION_VERTICAL, 4),
                                   frame_rate(30.0), osc_enabled(false), osc_server_port(7000), osc_rate(30)
{
    set_border_width(0);
    set_icon_name("beziercomposer");
    add(vbox);

    settings = Gio::Settings::create("de.wavel.beziercomposer");
    frame_rate = std::stoi(settings->get_string("guiplaybackfps"));
    osc_server_address = settings->get_string("oscserveraddress").c_str();
    osc_server_port = settings->get_int("oscserverport");
    osc_enabled = settings->get_boolean("enableosc");
    osc_rate = std::stoi(settings->get_string("oscupdaterate"));

    settings->signal_changed().connect(sigc::mem_fun(*this, &BezierComposer::on_setting_changed));

    std::cout << frame_rate << " " << osc_server_address << " " << osc_server_port
                << " " << osc_enabled << " " << osc_rate << std::endl;

    //  Create and connect a menu item from Stock (Quit)
    app->add_action("new", sigc::mem_fun(*this, &BezierComposer::on_new_file));
    app->add_action("open", sigc::mem_fun(*this, &BezierComposer::on_open_file));
    app->add_action("save", sigc::mem_fun(*this, &BezierComposer::on_save_file));
    app->add_action("save_as", sigc::mem_fun(*this, &BezierComposer::on_save_file));

    // keyboard shortcuts
    app->add_action("add_track", sigc::mem_fun(editor, &EditorArea::ctrl_add_track));
    app->add_action("add_curve", sigc::mem_fun(editor, &EditorArea::ctrl_add_curve));
    app->add_action("add_point", sigc::mem_fun(editor, &EditorArea::ctrl_add_point));
    app->add_action("remove_track", sigc::mem_fun(editor, &EditorArea::ctrl_remove_track));
    app->add_action("remove_curve", sigc::mem_fun(editor, &EditorArea::ctrl_remove_curve));
    app->add_action("remove_point", sigc::mem_fun(editor, &EditorArea::ctrl_remove_point));
    app->add_action("connect_points", sigc::mem_fun(editor, &EditorArea::ctrl_connect_points));
    app->add_action("disconnect_points", sigc::mem_fun(editor, &EditorArea::ctrl_disconnect_points));
    app->add_action("point_gate", sigc::mem_fun(editor, &EditorArea::ctrl_point_gate));

    app->add_action("toggle_play", sigc::mem_fun(*this, &BezierComposer::on_toggle_play));
    app->add_action("play", sigc::mem_fun(*this, &BezierComposer::on_play_button_pressed));
    app->add_action("pause", sigc::mem_fun(*this, &BezierComposer::on_pause_button_pressed));
    app->add_action("goto_start", sigc::mem_fun(*this, &BezierComposer::on_goto_start_button_pressed));

    app->add_action("copy", sigc::mem_fun(editor, &EditorArea::ctrl_copy));
    app->add_action("cut", sigc::mem_fun(editor, &EditorArea::ctrl_cut));
    app->add_action("paste", sigc::mem_fun(editor, &EditorArea::ctrl_paste));

    app->add_action("undo", sigc::mem_fun(editor, &EditorArea::ctrl_undo));
    app->add_action("redo", sigc::mem_fun(editor, &EditorArea::ctrl_redo));

    snap_action = app->add_action_bool("toggle_snap", sigc::mem_fun(*this, &BezierComposer::on_toggle_snap), false);
    loop_action = app->add_action_bool("toggle_loop", sigc::mem_fun(*this, &BezierComposer::on_toggle_loop), false);

    app->set_accel_for_action("app.open", "<Primary>o");
    app->set_accel_for_action("app.save", "<Primary>s");
    app->set_accel_for_action("app.save_as", "<Primary><Shift>s");

    app->set_accel_for_action("app.add_track", "<Primary>t");
    app->set_accel_for_action("app.add_curve", "<Primary>a");
    app->set_accel_for_action("app.add_point", "a");
    app->set_accel_for_action("app.remove_point", "Delete");
    app->set_accel_for_action("app.remove_curve", "<Primary>Delete");
    app->set_accel_for_action("app.connect_points", "c");
    app->set_accel_for_action("app.disconnect_points", "d");
    app->set_accel_for_action("app.point_gate", "g");
    app->set_accel_for_action("app.toggle_play", "space");
    app->set_accel_for_action("app.goto_start", "Home");
    app->set_accel_for_action("app.copy", "<Primary>c");
    app->set_accel_for_action("app.cut", "<Primary>x");
    app->set_accel_for_action("app.paste", "<Primary>v");
    app->set_accel_for_action("app.undo", "<Primary>z");
    app->set_accel_for_action("app.redo", "<Primary><Shift>z");
    app->set_accel_for_action("app.toggle_snap", "s");
    app->set_accel_for_action("app.toggle_loop", "l");


    auto refBuilder = Gtk::Builder::create();
    try
    {
        refBuilder->add_from_resource("/de/wavel/beziercomposer/app_menu.ui");
        refBuilder->add_from_resource("/de/wavel/beziercomposer/toolbar.glade");
    }
        catch (const Glib::Error& ex)
    {
        std::cerr << "ComposerApplication::on_startup(): " << ex.what() << std::endl;
        return;
    }

    auto object = refBuilder->get_object("appmenu");
    auto app_menu = Glib::RefPtr<Gio::MenuModel>::cast_dynamic(object);
    if (app_menu)
        app->set_menubar(app_menu);
    else
        std::cerr << "ComposerApplication::on_startup(): No \"appmenu\" object in app_menu.ui"
              << std::endl;
    //Get the toolbar and add it to a container widget:
    toolbar = nullptr;
    refBuilder->get_widget("toolbar", toolbar);
    if (!toolbar)
        g_warning("GtkToolbar not found");
    else {
        toolbar->set_toolbar_style(Gtk::TOOLBAR_ICONS);
        toolbar->set_icon_size(Gtk::ICON_SIZE_BUTTON);
        vbox.pack_start(*toolbar, Gtk::PACK_SHRINK);
    }

    vbox.pack_start(editor);
    vbox.show_all();
    set_title("BezierComposer");

    composition.bpm = 120;
    composition.sample_rate = 48000.0;

    editor.set_composition(&composition);

    jack_client = jack_client_open("BezierComposer", JackNullOption, NULL);

    std::cout << "jack_client" << jack_client << std::endl;
    editor.set_jack_client(jack_client);
    composition.jack_client = jack_client;

    // mixed output
    /*
    output_port_0 = jack_port_register(jack_client, "out_0", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    output_port_1 = jack_port_register(jack_client, "out_1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    */

    jack_set_process_callback(jack_client, &BezierComposer::jack_tick, (void *) this);
    jack_set_sample_rate_callback(jack_client, &BezierComposer::on_jack_sample_rate, (void *) this);

    jack_activate(jack_client);

    on_new_file();

    gui_timeout_slot = sigc::mem_fun(*this, &BezierComposer::on_timeout);
    osc_timeout_slot = sigc::mem_fun(*this, &BezierComposer::on_osc_timeout);
    gui_conn = Glib::signal_timeout().connect(gui_timeout_slot, 1000.0/frame_rate);
    if (osc_enabled)
        osc_conn = Glib::signal_timeout().connect(osc_timeout_slot, 1000.0/osc_rate);
}

BezierComposer::~BezierComposer()
{
}

BezierComposer* BezierComposer::create(const Glib::RefPtr<Gtk::Application>& app)
{
    return new BezierComposer(app);
}

void BezierComposer::on_goto_start_button_pressed()
{
    jack_transport_locate(jack_client, 0);
}

void BezierComposer::on_play_button_pressed()
{
    if (jack_client_state == JackTransportStopped) {
        std::cout << "Jack Transport Start" << std::endl;
        jack_transport_start(jack_client);
    }
}

void BezierComposer::on_pause_button_pressed()
{
    if (jack_client_state == JackTransportStarting || jack_client_state == JackTransportRolling) {
        std::cout << "Stop playing" << std::endl;
        jack_transport_stop(jack_client);
    }
}


void BezierComposer::on_toggle_play()
{
    if (jack_client_state == JackTransportStopped) {
        std::cout << "Jack Transport Start" << std::endl;
        jack_transport_start(jack_client);
    }
    else if (jack_client_state == JackTransportStarting || jack_client_state == JackTransportRolling) {
        std::cout << "Stop playing" << std::endl;
        jack_transport_stop(jack_client);
    }
}

void BezierComposer::on_toggle_snap()
{
    bool active = false;
    snap_action->get_state(active);
    snap_action->change_state(!active);
    editor.set_snap(!active);
}

void BezierComposer::on_toggle_loop()
{
    bool active = false;
    loop_action->get_state(active);
    loop_action->change_state(!active);
    composition.loop = !active;
}

void BezierComposer::on_new_file()
{
    auto content = Gio::Resource::lookup_data_global("/de/wavel/beziercomposer/startup.bezier");
    pugi::xml_document doc;
    gsize content_size = content->get_size();
    pugi::xml_parse_result result = doc.load_buffer(content->get_data(content_size), content_size);
    if (result)
        setup_loaded_xml(doc);
    else
        std::cout << "Error while parsing";

}

void BezierComposer::on_open_file()
{
    Gtk::FileChooserDialog dialog = Gtk::FileChooserDialog("Select curve file", Gtk::FILE_CHOOSER_ACTION_OPEN);
    dialog.set_transient_for(*this);

    dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
    dialog.add_button("Open", Gtk::RESPONSE_OK);

    auto filter_bezier = Gtk::FileFilter::create();
    filter_bezier->set_name("BezierComposer files");
    filter_bezier->add_pattern("*.bezier");
    dialog.add_filter(filter_bezier);

    auto filter_any = Gtk::FileFilter::create();
    filter_any->set_name("Any files");
    filter_any->add_pattern("*");
    dialog.add_filter(filter_any);

    int result = dialog.run();
    switch(result)
    {
    case (Gtk::RESPONSE_CANCEL):
        break;
    case (Gtk::RESPONSE_OK):
        load_file(dialog.get_file());
        file_ptr = dialog.get_file();
        set_title("BezierComposer [" + file_ptr->get_basename() + "]");
        break;
    }
}

void BezierComposer::on_save_file()
{
    if (!file_ptr) {
        on_save_file_as();
        return;
    }
    save_file(file_ptr);
}

void BezierComposer::on_save_file_as()
{
    Gtk::FileChooserDialog dialog = Gtk::FileChooserDialog("Save to curve file", Gtk::FILE_CHOOSER_ACTION_SAVE);
    dialog.set_transient_for(*this);

    dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
    dialog.add_button("Save", Gtk::RESPONSE_OK);

    auto filter_bezier = Gtk::FileFilter::create();
    filter_bezier->set_name("BezierComposer files");
    filter_bezier->add_pattern("*.bezier");
    dialog.add_filter(filter_bezier);

    auto filter_any = Gtk::FileFilter::create();
    filter_any->set_name("Any files");
    filter_any->add_pattern("*");
    dialog.add_filter(filter_any);

    int result = dialog.run();
    switch(result)
    {
    case (Gtk::RESPONSE_CANCEL):
        break;
    case (Gtk::RESPONSE_OK):
        auto file = dialog.get_file();
        if (!file->query_exists()) {
            // check if file ending is correct
            bool append_ending = false;
            std::string ending = ".bezier";
            std::string filename = file->get_parse_name();
            if (filename.length() < ending.length())
                append_ending = true;
            else {
                append_ending = !(0 == filename.compare (filename.length() - ending.length(), ending.length(), ending));
            }
            if (append_ending) {
                file = Gio::File::create_for_path(file->get_parse_name() + ".bezier");
            }
        }
        save_file(file);
        file_ptr = file;
        set_title("BezierComposer [" + file_ptr->get_basename() + "]");
        break;
    }
}

void BezierComposer::load_file(const Glib::RefPtr<Gio::File>& file)
{
    try {
        char* contents = nullptr;
        gsize length = 0;

        file->load_contents(contents, length);
        std::cout << contents << std::endl;
        pugi::xml_document doc;
        pugi::xml_parse_result result = doc.load_file(file->get_path().c_str());
        if (result)
            setup_loaded_xml(doc);
        else
            std::cout << "Error while parsing";
        file_ptr = file;
    }
    catch (const Glib::Error& ex)
    {
        std::cout << file->get_parse_name() << ":\n" << ex.what() << std::endl;
    }
}

void BezierComposer::setup_loaded_xml(const pugi::xml_document& doc) {
    composition.from_xml(doc);
    composition.clear_history();

    editor.reset_selection();
    editor.queue_draw();
}

void BezierComposer::save_file(const Glib::RefPtr<Gio::File>& file)
{
    pugi::xml_document doc = composition.to_xml();
    std::cout << "Saving result: " << doc.save_file(file->get_path().c_str()) << std::endl;
    std::cout << "Document:\n";
    doc.save(std::cout);
}

bool BezierComposer::on_timeout()
{
    jack_position_t current_pos;
    jack_transport_state_t transport_state;

    transport_state = jack_transport_query(jack_client, &current_pos);
    if (current_pos.valid && current_pos.beats_per_minute != composition.bpm)
        composition.bpm = current_pos.beats_per_minute;
    frame_time = jack_get_current_transport_frame(jack_client);

    switch(transport_state) {
	case JackTransportRolling:
	{
        double pos = (frame_time / composition.sample_rate) * (composition.bpm / 60);
        if (composition.loop) {
            double length = composition.get_length();
            if (length > 0.01)
                pos = std::fmod(pos, length);
        }
        editor.set_playhead_pos(pos);
        queue_draw();
		break;
    }
    case JackTransportStopped:
	case JackTransportStarting:
	default:
        break;
	}
	jack_client_state = transport_state;
	composition.garbage_collect();
	return true;
}

bool BezierComposer::on_osc_timeout()
{
    if (!osc_enabled) {
        return false;
    }
    jack_position_t current_pos;

    prev_osc_transport_state = osc_transport_state;
    osc_transport_state = jack_transport_query(jack_client, &current_pos);
    prev_osc_frame_time = osc_frame_time;
    osc_frame_time = jack_get_current_transport_frame(jack_client);

    if (composition.loop) {
        double length = composition.get_length();
        if (length > 0.01)
            osc_frame_time = get_frame(std::fmod(get_time(osc_frame_time), length));
    }

    switch(osc_transport_state) {
    case JackTransportStopped:
        if (prev_osc_transport_state != osc_transport_state)
            osc_stop_all_notes();
        break;
	case JackTransportRolling:
        send_osc_msgs();
		break;
	case JackTransportStarting:
	default:
        break;
	}
	return true;
}

void BezierComposer::send_osc_msgs()
{
    int OUTPUT_BUFFER_SIZE = 1024;
    char buffer[OUTPUT_BUFFER_SIZE];
    UdpTransmitSocket transmitSocket( IpEndpointName( osc_server_address, osc_server_port ) );
    osc::OutboundPacketStream p( buffer, OUTPUT_BUFFER_SIZE );

    p << osc::BeginBundleImmediate;

    std::list<BezierTrack*>::iterator it = composition.tracks.begin();
    for (unsigned int pos=0; it != composition.tracks.end(); pos++) {
        bool available;
        double midi_note;
        double frequency;

        std::tie(available, midi_note) = (*it)->get_value(get_time(osc_frame_time));
        frequency = BezierTrack::midi_to_freq(midi_note);
        if ( available ) {
            std::stringstream path;
            path << "/channel" << pos << "/gate";
            if ((*it)->contains_gate(get_time(prev_osc_frame_time), get_time(osc_frame_time))) { // re-gate
                p << osc::BeginMessage( path.str().c_str()) << 0 << osc::EndMessage;
                p << osc::BeginMessage( path.str().c_str()) << 1 << osc::EndMessage;
            }
            else if ((*it)->last_gate != 1) { //note not playing yet
                p << osc::BeginMessage( path.str().c_str()) << 1 << osc::EndMessage;
                (*it)->last_gate = 1;
            }
            path.str("");
            path.clear();
            path << "/channel" << pos << "/freq";
            p << osc::BeginMessage( path.str().c_str()) << (float) frequency << osc::EndMessage;
        }
        else {
            if ((*it)->last_gate != 0) {
                (*it)->last_gate = 0;
                std::stringstream path;
                path << "/channel" << pos << "/gate";
                p << osc::BeginMessage( path.str().c_str()) << 0 << osc::EndMessage;
            }
        }
        it++;
    }
    p << osc::EndBundle;
    transmitSocket.Send( p.Data(), p.Size() );
};

void BezierComposer::osc_stop_all_notes()
{
    int OUTPUT_BUFFER_SIZE = 1024;
    char buffer[OUTPUT_BUFFER_SIZE];
    UdpTransmitSocket transmitSocket( IpEndpointName( osc_server_address, osc_server_port ) );
    osc::OutboundPacketStream p( buffer, OUTPUT_BUFFER_SIZE );

    p << osc::BeginBundleImmediate;

    std::list<BezierTrack*>::iterator it = composition.tracks.begin();
    for (unsigned int pos=0; it != composition.tracks.end(); pos++) {
        (*it)->last_gate = 0;
        std::stringstream path;
        path << "/channel" << pos << "/gate";
        p << osc::BeginMessage( path.str().c_str()) << 0 << osc::EndMessage;
        it++;
    }
    p << osc::EndBundle;
    transmitSocket.Send( p.Data(), p.Size() );
}

int BezierComposer::on_jack_sample_rate(jack_nframes_t nframes, void *arg)
{
    BezierComposer *composer = (BezierComposer *) arg;
    composer->composition.sample_rate = nframes;
    std::cout << "Set sample rate: " << composer->composition.sample_rate << std::endl;
    return 0;
}

int BezierComposer::jack_tick( jack_nframes_t nframes, void *dataPointer )
{
    BezierComposer *composer = (BezierComposer *) dataPointer;

    jack_position_t current_pos;
    jack_transport_state_t transport_state;
    jack_nframes_t frame_t;

    transport_state = jack_transport_query(composer->jack_client, &current_pos);
    frame_t = jack_get_current_transport_frame(composer->jack_client);

    /*
    while (composer->sines.size() < composer->composition.tracks.size())
        composer->sines.insert(composer->sines.end(), stk::BlitSaw());
    std::list<stk::BlitSaw>::iterator sineit = composer->sines.begin();
    */

    std::list<BezierTrack*>::iterator it = composer->composition.tracks.begin();
    jack_default_audio_sample_t *freq_snd_samples = nullptr;
    jack_default_audio_sample_t *freq_cv_samples = nullptr;
    jack_default_audio_sample_t *gate_cv_samples = nullptr;
    jack_default_audio_sample_t *trig_cv_samples = nullptr;

    double length_frames = composer->get_frame(composer->composition.get_length());
    jack_nframes_t cur_frame = 0;

    bool available = false;
    double midi_note = 0;
    double frequency = 0;
    bool contains_gate = false;
    bool last_available = false;
    double last_midi_note = 0;
    bool last_contains_gate = false;
    while (it != composer->composition.tracks.end()) {
        bool use_freq_snd = (bool) (*it)->freq_snd_port;
        bool use_freq_cv = (bool) (*it)->freq_cv_port;
        bool use_gate_cv = (bool) (*it)->gate_cv_port;
        bool use_trig_cv = (bool) (*it)->trig_cv_port;
        if (use_freq_snd)
            freq_snd_samples = (jack_default_audio_sample_t*)jack_port_get_buffer((*it)->freq_snd_port, nframes);
        if (use_freq_cv)
            freq_cv_samples = (jack_default_audio_sample_t*)jack_port_get_buffer((*it)->freq_cv_port, nframes);
        if (use_gate_cv)
            gate_cv_samples = (jack_default_audio_sample_t*)jack_port_get_buffer((*it)->gate_cv_port, nframes);
        if (use_trig_cv)
            trig_cv_samples = (jack_default_audio_sample_t*)jack_port_get_buffer((*it)->trig_cv_port, nframes);
        int RECALC_EVERY = 20;
        for ( unsigned int i=0; i<nframes; i++ ) {
            if (transport_state == JackTransportRolling) {
                if (i%RECALC_EVERY == 0) { // compute frequency only every n frames to save time
                    if (composer->composition.loop && length_frames > 10)
                        cur_frame = std::fmod((frame_t+i), length_frames);
                    else
                        cur_frame = frame_t+i;
                    std::tie(available, midi_note) = (*it)->get_value(composer->get_time(cur_frame));
                    frequency = BezierTrack::midi_to_freq(midi_note);
                    last_available = available;
                    last_midi_note = midi_note;
                    contains_gate = (*it)->contains_gate(composer->get_time(cur_frame - 1), composer->get_time(cur_frame + 2 + RECALC_EVERY));
                    last_contains_gate = contains_gate;
                }
                else {
                    available = last_available;
                    midi_note = last_midi_note;
                    contains_gate = last_contains_gate;
                }
                // double amp = 0.2;
                if ( available ) {
                    // (*sineit).setFrequency(frequency);
                    (*it)->last_freq = frequency;
                }
                else {
                    if ((*it)->last_freq > 0) {
                        // (*sineit).setFrequency((*it)->last_freq);
                        frequency = (*it)->last_freq;
                    }
                    else {
                        frequency = 0;
                    }
                }
                if (use_freq_snd) *freq_snd_samples++ = 0; // = (*sineit).tick() * amp;
                if (use_freq_cv) *freq_cv_samples++ = ((*it)->freq_to_midi(frequency)) / 12.0;
                if (contains_gate) {
                    if (use_trig_cv) *trig_cv_samples++ = 1;
                    if (use_gate_cv) *gate_cv_samples++ = 0;
                }
                else {
                    if (use_trig_cv) *trig_cv_samples++ = 0;
                    if (available) {
                        if (use_gate_cv) *gate_cv_samples++ = 1;
                    }
                    else {
                        if (use_gate_cv) *gate_cv_samples++ = 0;
                    }
                }

            }
            else {
                if (use_freq_snd) *freq_snd_samples++ = 0;
                if (use_freq_cv) *freq_cv_samples++ = 0;
                if (use_gate_cv) *gate_cv_samples++ = 0;
                if (use_trig_cv) *trig_cv_samples++ = 0;
            }
        }
        it++;
        // sineit++;
    }

    if (composer->output_port_0 && composer->output_port_1) {
        jack_default_audio_sample_t *samples_0;
        jack_default_audio_sample_t *samples_1;
        samples_0 = (jack_default_audio_sample_t*)jack_port_get_buffer(composer->output_port_0, nframes);
        samples_1 = (jack_default_audio_sample_t*)jack_port_get_buffer(composer->output_port_1, nframes);
        for ( unsigned int i=0; i<nframes; i++ ) {
            *samples_0++ = 0;
            *samples_1++ = 0;
        }
    }
    return 0;
}

void BezierComposer::on_setting_changed(Glib::ustring key) {
    if (key == "guiplaybackfps") {
        frame_rate = std::stoi(settings->get_string("guiplaybackfps"));
        if (gui_conn.connected())
            gui_conn.disconnect();
        std::cout << "Reconnect timeout with new framerate " << frame_rate << std::endl;
        gui_conn = Glib::signal_timeout().connect(gui_timeout_slot, 1000.0/frame_rate);
    }
    else if (key == "oscserveraddress")
        osc_server_address = settings->get_string("oscserveraddress").c_str();
    else if (key == "oscserverport")
        osc_server_port = settings->get_int("oscserverport");
    else if (key == "enableosc") {
        osc_enabled = settings->get_boolean("enableosc");
        if (osc_enabled && !osc_conn.connected())
            osc_conn = Glib::signal_timeout().connect(osc_timeout_slot, 1000.0/osc_rate);
    }
    else if (key == "oscupdaterate") {
        osc_rate = std::stoi(settings->get_string("oscupdaterate"));
        if (osc_conn.connected())
            osc_conn.disconnect();
        if (osc_enabled) {
            std::cout << "Reconnect timeout with new OSC rate " << osc_rate << std::endl;
            osc_conn = Glib::signal_timeout().connect(osc_timeout_slot, 1000.0/osc_rate);
        }
    }
}

double BezierComposer::get_time(jack_nframes_t frame_time)
{
    return (frame_time / composition.sample_rate) * (composition.bpm/60);
}

jack_nframes_t BezierComposer::get_frame(double time)
{
    return (time * composition.sample_rate) / (composition.bpm/60);
}

