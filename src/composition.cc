/*
 * Copyright (C) 2021 Valentin Pratz <git@valentinpratz.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "composition.h"
#include <iostream>

Composition::Composition() : loop(false), deleted_points(), deleted_curves(), deleted_tracks(), garbage_counter(0),
                             history(), history_pos(0)
{
};

BezierTrack* Composition::create_track(bool register_ports, long id, int position)
{
    if (id < 0 || tid.count(id) > 0)
        id = next_track_id();
    BezierTrack* t = new BezierTrack(id, jack_client, register_ports);
    tid[id] = t;
    return t;
}

void Composition::remove_track(long track_id, bool close_ports)
{
    if (tid.count(track_id) == 0)
        return;
    BezierTrack &track = *(tid.at(track_id));
    std::list<BezierCurve*>::iterator c_it = track.curves.begin();
    while (c_it != track.curves.end())
        deconstruct_curve((*c_it++)->id);
    std::list<BezierTrack*>::iterator t_it = tracks.begin();
    while (t_it != tracks.end()) {
        if ((*t_it)->id == track_id) {
            tracks.erase(t_it);
            break;
        }
        t_it++;
    }
    if (close_ports) {
        tid[track_id]->unregister_jack_ports();
    }
    deleted_tracks.push_back(tid[track_id]);
    garbage_counter = 0;
    tid.erase(track_id);
}

BezierCurve* Composition::create_curve(long id)
{
    if (id < 0 || cid.count(id) > 0)
        id = next_curve_id();
    BezierCurve* curve = new BezierCurve(id);
    cid[id] = curve;
    return curve;
}

void Composition::remove_curve(long curve_id)
{
    if (cid.count(curve_id) == 0)
        return;
    for (const auto& t : tid) {
        std::list<BezierCurve*>::iterator it = t.second->curves.begin();
        while (it != t.second->curves.end()) {
            if ((*it)->id == curve_id) {
                t.second->curves.erase(it);
                break;
            }
            it++;
        }
    }
    deconstruct_curve(curve_id);
}

void Composition::deconstruct_curve(long curve_id)
{
    BezierCurve &curve = *(cid.at(curve_id));
    std::list<BezierPoint*>::iterator it = curve.points.begin();
    while (it != curve.points.end()) {
        deconstruct_point((*it++)->id);
    }
    deleted_curves.push_back(cid[curve_id]);
    garbage_counter = 0;
    cid.erase(curve_id);
}

BezierPoint* Composition::create_point(long id)
{
    if (id < 0 || pid.count(id) > 0)
        id = next_point_id();
    BezierPoint* point = new BezierPoint(id);
    pid[id] = point;
    return point;
}

BezierPoint* Composition::create_point(double x, double y, double pre_rel_x, double pre_rel_y, double post_rel_x, double post_rel_y, long id)
{
    if (id < 0 || pid.count(id) > 0)
        id = next_point_id();
    BezierPoint* point = new BezierPoint(id, x, y, pre_rel_x, pre_rel_y, post_rel_x, post_rel_y);
    pid[id] = point;
    return point;
}

void Composition::remove_point(long point_id)
{
    if (pid.count(point_id) == 0)
        return;
    auto copy_cid(cid);
    for (const auto& c : copy_cid) {
        std::list<BezierPoint*>::iterator it = c.second->points.begin();
        while (it != c.second->points.end()) {
            if ((*it)->id == point_id) {
                c.second->points.erase(it);
                 if (c.second->points.empty()) // curve empty -> remove curve
                    remove_curve(c.first);
                break;
            }
            it++;
        }
    }
    deconstruct_point(point_id);
}

void Composition::deconstruct_point(long point_id)
{
    deleted_points.push_back(pid[point_id]);
    garbage_counter = 0;
    pid.erase(point_id);
}

BezierPoint* Composition::add_point_at(double time, long curve_id)
{
    BezierCurve &curve = *(cid[curve_id]);
    double handle_dist = 0.4;
    if (curve.points.size() == 0) {
        BezierPoint* p = create_point(time, 60, -handle_dist, 0.0, handle_dist, 0.0);
        curve.points.insert(curve.points.begin(), p);
        return p;
    }
    else if (time < curve.points.front()->x) {
        BezierPoint* p = create_point(time, curve.points.front()->y, -handle_dist, 0.0, handle_dist, 0.0);
        curve.points.insert(curve.points.begin(), p);
        return p;
    }
    else if (time > curve.points.back()->x) {
        BezierPoint* p = create_point(time, curve.points.back()->y, -handle_dist, 0.0, handle_dist, 0.0);
        curve.points.insert(curve.points.end(), p);
        return p;
    }
    else {
        return subdivide_curve(time, curve_id);
    }
}

BezierPoint* Composition::add_point_at(double time, double value, long curve_id)
{
    BezierCurve &curve = *(cid[curve_id]);
    double handle_dist = 0.4;
    BezierPoint* p = create_point(time, value, -handle_dist, 0.0, handle_dist, 0.0);
    if (curve.points.size() == 0 || time < curve.points.front()->x) {
        curve.points.insert(curve.points.begin(), p);
    }
    else if (time > curve.points.back()->x) {
        curve.points.insert(curve.points.end(), p);
    }
    else {
        std::list<BezierPoint*>::iterator it = curve.points.begin();
        while (it != curve.points.end())
        {
            BezierPoint* end_point = *it;
            if (time < end_point->x) {
                curve.points.insert(it, p);
                break;
            }
            it++;
        }
    }
    return p;
}

BezierCurve* Composition::add_curve_at(double time, long track_id)
{
    BezierTrack* track = tid[track_id];
    BezierCurve* curve = create_curve();
    add_point_at(time, curve->id);
    track->curves.push_back(curve);
    return curve;
}

BezierCurve* Composition::add_curve_at(double time, double value, long track_id)
{
    BezierTrack* track = tid[track_id];
    BezierCurve* curve = create_curve();
    add_point_at(time, value, curve->id);
    track->curves.push_back(curve);
    return curve;
}

BezierPoint* Composition::subdivide_curve(double x, long curve_id)
{
    // x = time
    BezierCurve &curve = *(cid.at(curve_id));
    if (curve.points.size() < 2)
        return nullptr;
    std::list<BezierPoint*>::iterator it = curve.points.begin();
    BezierPoint* start_point = *it++;
    if (x < start_point->x )
        return nullptr;
    while (it != curve.points.end())
    {
        BezierPoint &end_point = **it;
        if (x < end_point.x) {
            bool found;
            double t;
            std::tie(found, t) = BezierCurve::t_of_x(x, *start_point, end_point);
            if (!found)
                return nullptr;
            double ax0, bx0, cx0, dx0, ax1, bx1, cx1, dx1;
            double ay0, by0, cy0, dy0, ay1, by1, cy1, dy1;
            double Ax = start_point->x;
            double Bx = start_point->x + start_point->post_rel_x;
            double Cx = end_point.x + end_point.pre_rel_x;
            double Dx = end_point.x;
            double Ay = start_point->y;
            double By = start_point->y + start_point->post_rel_y;
            double Cy = end_point.y + end_point.pre_rel_y;
            double Dy = end_point.y;
            std::tie(ax0, bx0, cx0, dx0, ax1, bx1, cx1, dx1) = BezierCurve::calc_subdiv(Ax, Bx, Cx, Dx, t);
            std::tie(ay0, by0, cy0, dy0, ay1, by1, cy1, dy1) = BezierCurve::calc_subdiv(Ay, By, Cy, Dy, t);

            BezierPoint &new_point = *create_point();

            start_point->post_rel_x = bx0 - start_point->x;
            new_point.x = dx0;
            new_point.pre_rel_x = cx0 - new_point.x;
            new_point.post_rel_x = bx1 - new_point.x;
            end_point.pre_rel_x = cx1 - end_point.x;

            start_point->post_rel_y = by0 - start_point->y;
            new_point.y = dy0;
            new_point.pre_rel_y = cy0 - new_point.y;
            new_point.post_rel_y = by1 - new_point.y;
            end_point.pre_rel_y = cy1 - end_point.y;

            curve.points.insert(it, &new_point);
            return &new_point;
        }
        start_point = &end_point;
        it++;
    }
    return nullptr;
}

double Composition::get_length()
{
    // returns position of last point in beats, needed for loop
    double max_val = 0;
    for (const auto& p : pid) {
        if (p.second->x > max_val)
            max_val = p.second->x;
    }
    return max_val;
}


long Composition::next_track_id()
{
    long pos = 0;
    for (const auto& p : tid) {
        if (pos != p.first)
            return pos;
        pos++;
    }
    return pos;
}

long Composition::next_curve_id()
{
    long pos = 0;
    for (const auto& p : cid) {
        if (pos != p.first)
            return pos;
        pos++;
    }
    return pos;
}

long Composition::next_point_id()
{
    long pos = 0;
    for (const auto& p : pid) {
        if (pos != p.first)
            return pos;
        pos++;
    }
    return pos;
}

void Composition::clear_history()
{
    history_pos = 0;
    history.clear();
    store_history();
}

void Composition::store_history()
{
    int history_size = 20;
    if (history_pos != 0) {
        auto it = history.begin();
        for (int pos = 0; pos < history_pos; pos++) {
            history.erase(it++);
        }
        history_pos = 0;
    }
    history.insert(history.begin(), to_xml());
    while ((int) history.size() > history_size + 1)
        history.erase(--(history.end()));
}

bool Composition::undo()
{
    if (history_pos + 1 < (int) history.size()) {
        auto it = history.begin();
        std::advance(it, ++history_pos);
        from_xml(*it);
        return true;
    }
    return false;
}

bool Composition::redo()
{
    if (history_pos > 0 && history_pos < (int) history.size()) {
        auto it = history.begin();
        std::advance(it, --history_pos);
        from_xml(*it);
        return true;
    }
    return false;
}

void Composition::garbage_collect(unsigned int age)
{
    // to avoid segfaults in the callbacks, points/tracks/curves should not be deleted immediately
    // -> delay by age calls to garbage_collect (has to be called in one of the loop functions (timeout)
    // reset garbage_counter when adding to deleted_xxx
    if (garbage_counter < age && (!deleted_points.empty() || !deleted_curves.empty() || !deleted_tracks.empty()) )
        garbage_counter++;
    else if (!deleted_points.empty() || !deleted_curves.empty() || !deleted_tracks.empty()) {
        // all deletions are old -> deconstruct
        auto p_it = deleted_points.begin();
        while (p_it != deleted_points.end()) {
            delete &**p_it;
            deleted_points.erase(p_it++);
        }
        auto c_it = deleted_curves.begin();
        while (c_it != deleted_curves.end()) {
            delete &**c_it;
            deleted_curves.erase(c_it++);
        }
        auto t_it = deleted_tracks.begin();
        while (t_it != deleted_tracks.end()) {
            delete &**t_it;
            deleted_tracks.erase(t_it++);
        }
        garbage_counter = 0;
    }
}

void Composition::from_xml(const pugi::xml_document& doc)
{
    pugi::xml_node comp = doc.child("composition");
    if (comp.type() == pugi::node_null) {
        std::cout << "Error loading file: root node \"composition\" not found" << std::endl;
        return;
    }
    bpm = comp.attribute("bpm").as_double();

    // store jack connections for reuse
    std::list<std::tuple<jack_port_t*, jack_port_t*, jack_port_t*, jack_port_t*>> old_cons;
    std::list<BezierTrack*> old_tracks(tracks); // copy it because remove_track changes tracks
    std::list<BezierTrack*>::iterator old_it = old_tracks.begin();
    while (old_it != old_tracks.end()) {
        if (((*old_it)->freq_cv_port)) {
            old_cons.push_back(std::make_tuple((*old_it)->freq_snd_port, (*old_it)->freq_cv_port, (*old_it)->gate_cv_port, (*old_it)->trig_cv_port));
        }
        remove_track((*old_it++)->id, false);
    }

    if (tid.size() > 0)
        std::cout << "Cleanup failed, tracks left" << std::endl;
    if (cid.size() > 0)
        std::cout << "Cleanup failed, curves left" << std::endl;
    if (pid.size() > 0)
        std::cout << "Cleanup failed, points left" << std::endl;

    for (pugi::xml_node track_node = comp.child("track"); track_node; track_node = track_node.next_sibling("track")) {
        BezierTrack &track = *(create_track(false, track_node.attribute("id").as_int(-1)));
        track.type = (BezierTrack::Type) track_node.attribute("type").as_int();
        track.channel = track_node.attribute("channel").as_int();
        track.height = track_node.attribute("height").as_double();
        track.y_min = track_node.attribute("y_min").as_double();
        track.y_max = track_node.attribute("y_max").as_double();

        for (pugi::xml_node curve_node = track_node.child("curve"); curve_node; curve_node = curve_node.next_sibling("curve")) {
            BezierCurve* curve = create_curve(curve_node.attribute("id").as_int(-1));
            for (pugi::xml_node p_node = curve_node.child("point"); p_node; p_node = p_node.next_sibling("point")) {
                BezierPoint* p = create_point(
                    p_node.attribute("x").as_double(),
                    p_node.attribute("y").as_double(),
                    p_node.attribute("pre_rel_x").as_double(),
                    p_node.attribute("pre_rel_y").as_double(),
                    p_node.attribute("post_rel_x").as_double(),
                    p_node.attribute("post_rel_y").as_double(),
                    p_node.attribute("id").as_int(-1)
                );
                p->gate = p_node.attribute("gate").as_bool();
                curve->points.push_back(p);
            }
            track.curves.push_back(curve);
        }
        tracks.push_back(&track);
    }
    std::list<BezierTrack*>::iterator it = tracks.begin();
    std::list<std::tuple<jack_port_t*, jack_port_t*, jack_port_t*, jack_port_t*>>::iterator old_cons_it = old_cons.begin();
    // TODO: set names correctly
    while (it != tracks.end() && old_cons_it != old_cons.end()) {
        (*it)->freq_snd_port = std::get<0>(*old_cons_it);
        (*it)->freq_cv_port = std::get<1>(*old_cons_it);
        (*it)->gate_cv_port = std::get<2>(*old_cons_it);
        (*it)->trig_cv_port = std::get<3>(*old_cons_it);
        it++;
        old_cons_it++;
    }

    while (old_cons_it != old_cons.end()) {
        jack_port_unregister(jack_client, std::get<0>(*old_cons_it));
        jack_port_unregister(jack_client, std::get<1>(*old_cons_it));
        jack_port_unregister(jack_client, std::get<2>(*old_cons_it));
        jack_port_unregister(jack_client, std::get<3>(*old_cons_it));
        old_cons_it++;
    }

    while (it != tracks.end()) {
        (*it++)->register_jack_ports();
    }
}

pugi::xml_document Composition::to_xml()
{
    pugi::xml_document doc;
    pugi::xml_node comp = doc.append_child("composition");
    comp.append_attribute("bpm").set_value(bpm);

    std::list<BezierTrack*>::iterator track_it = tracks.begin();
    while (track_it != tracks.end()) {
        BezierTrack tr = **track_it++;
        pugi::xml_node track_node = comp.append_child("track");
        track_node.append_attribute("id").set_value((long long) tr.id);
        track_node.append_attribute("type").set_value((int) tr.type);
        track_node.append_attribute("channel").set_value(tr.channel);
        track_node.append_attribute("height").set_value(tr.height);
        track_node.append_attribute("y_min").set_value(tr.y_min);
        track_node.append_attribute("y_max").set_value(tr.y_max);

        std::list<BezierCurve*>::iterator curve_it = tr.curves.begin();
        while (curve_it != tr.curves.end()) {
            BezierCurve curve = **curve_it++;
            pugi::xml_node curve_node = track_node.append_child("curve");
            curve_node.append_attribute("id").set_value((long long) curve.id);

            std::list<BezierPoint*>::iterator points_it = curve.points.begin();
            while (points_it != curve.points.end()) {
                BezierPoint &p = **points_it++;
                pugi::xml_node point_node = curve_node.append_child("point");
                point_node.append_attribute("id").set_value((long long) p.id);
                point_node.append_attribute("x").set_value(p.x);
                point_node.append_attribute("y").set_value(p.y);
                point_node.append_attribute("pre_rel_x").set_value(p.pre_rel_x);
                point_node.append_attribute("pre_rel_y").set_value(p.pre_rel_y);
                point_node.append_attribute("post_rel_x").set_value(p.post_rel_x);
                point_node.append_attribute("post_rel_y").set_value(p.post_rel_y);
                point_node.append_attribute("gate").set_value(p.gate);
            }
        }
    }
    return doc;
}
