/*
 * Copyright (C) 2021 Valentin Pratz <git@valentinpratz.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef COMPOSERAPPLICATION_H
#define COMPOSERAPPLICATION_H

#include <gtkmm.h>

class BezierComposer;

class ComposerApplication: public Gtk::Application
{
protected:
  ComposerApplication();

public:
  static Glib::RefPtr<ComposerApplication> create();

protected:
  // Override default signal handlers:
  void on_startup() override;
  void on_activate() override;

private:
  BezierComposer* create_appwindow();
  void on_hide_window(Gtk::Window* window);
  void on_action_preferences();
  void on_action_quit();
};

#endif /* COMPOSERAPPLICATION_H */
