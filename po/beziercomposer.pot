# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Valentin Pratz
# This file is distributed under the same license as the beziercomposer package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: beziercomposer 0.1.0\n"
"Report-Msgid-Bugs-To: beziercomposer@wavel.de\n"
"POT-Creation-Date: 2020-12-30 18:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: res/app_menu.ui:6
msgid "_File"
msgstr ""

#: res/app_menu.ui:9
msgid "_New"
msgstr ""

#: res/app_menu.ui:15
msgid "_Open"
msgstr ""

#: res/app_menu.ui:19
msgid "_Save"
msgstr ""

#: res/app_menu.ui:23
msgid "_Save _As"
msgstr ""

#: res/app_menu.ui:29
msgid "_Quit"
msgstr ""

#: res/app_menu.ui:35
msgid "_Edit"
msgstr ""

#: res/app_menu.ui:38
msgid "_Cut"
msgstr ""

#: res/app_menu.ui:42
msgid "_Copy"
msgstr ""

#: res/app_menu.ui:46
msgid "_Paste"
msgstr ""

#: res/app_menu.ui:52
msgid "_Preferences"
msgstr ""

#: res/app_menu.ui:58
msgid "_Track"
msgstr ""

#: res/app_menu.ui:61
msgid "_Add"
msgstr ""

#: res/app_menu.ui:65
msgid "_Remove"
msgstr ""

#: res/toolbar.glade:37
msgid "Go To Start"
msgstr ""

#: res/toolbar.glade:50
msgid "Play/Pause"
msgstr ""

#: res/toolbar.glade:86
msgid "Add Point"
msgstr ""

#: res/toolbar.glade:100
msgid "Remove point"
msgstr ""

#: res/toolbar.glade:114
msgid "Add Curve"
msgstr ""

#: res/toolbar.glade:151
msgid "Toggle Gate"
msgstr ""

#: res/toolbar.glade:165
msgid "Use snap"
msgstr ""

#: res/toolbar.glade:167
msgid "Snap"
msgstr ""

#: res/prefs.ui:14
msgid "Preferences"
msgstr ""

#: res/prefs.ui:66
msgid "1"
msgstr ""

#: res/prefs.ui:67
msgid "10"
msgstr ""

#: res/prefs.ui:68
msgid "15"
msgstr ""

#: res/prefs.ui:69
msgid "20"
msgstr ""

#: res/prefs.ui:70
msgid "25"
msgstr ""

#: res/prefs.ui:71 res/prefs.ui:199
msgid "30"
msgstr ""

#: res/prefs.ui:72
msgid "50"
msgstr ""

#: res/prefs.ui:73 res/prefs.ui:200
msgid "60"
msgstr ""

#: res/prefs.ui:93
msgid "General"
msgstr ""

#: res/prefs.ui:107
msgid "Enable _OSC"
msgstr ""

#: res/prefs.ui:117
msgid " "
msgstr ""

#: res/prefs.ui:145
msgid "127.0.0.1"
msgstr ""

#: res/prefs.ui:158
msgid "_OSC Server Port"
msgstr ""

#: res/prefs.ui:201
msgid "100"
msgstr ""

#: res/prefs.ui:202
msgid "250"
msgstr ""

#: res/prefs.ui:203
msgid "500"
msgstr ""

#: res/prefs.ui:204
msgid "1000"
msgstr ""

#: res/prefs.ui:221
msgid "OSC"
msgstr ""

#: beziercomposer.desktop.in:4
msgid "Bezier Composer"
msgstr ""

#: beziercomposer.desktop.in:5
msgid "BezierComposer"
msgstr ""

#: beziercomposer.desktop.in:6
msgid "Compose for CV/Gate synths using bezier curves"
msgstr ""

#: beziercomposer.desktop.in:7
msgid "beziercomposer"
msgstr ""
